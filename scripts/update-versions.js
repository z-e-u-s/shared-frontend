const fs = require('fs');
const path = require('path');

// get package.json and package-lock.json from host application
const packageJsonPath = path.join(__dirname, '../package.json');
const packageLockJsonPath = path.join(__dirname, '../package-lock.json');
const packageJson = require(packageJsonPath);
const packageLockJson = require(packageLockJsonPath);

// get package.json and package-lock.json from shared library
const libPackageJsonPath = path.join(__dirname, '../projects/shared/package.json');
const libPackageLockJsonPath = path.join(__dirname, '../projects/shared/package-lock.json');
const libPackageJson = require(libPackageJsonPath);
const libPackageLockJson = require(libPackageLockJsonPath);

// update patch version
const versionParts = packageJson.version.split('.');
const newPatchVersion = parseInt(versionParts[2], 10) + 1;
versionParts[2] = newPatchVersion;
packageJson.version = versionParts.join('.');
packageLockJson.version = versionParts.join('.');
libPackageJson.version = versionParts.join('.');
libPackageLockJson.version = versionParts.join('.');

// write updated version to host application files
fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2) + '\n');
fs.writeFileSync(packageLockJsonPath, JSON.stringify(packageLockJson, null, 2) + '\n');

// write updated version to shared library files
fs.writeFileSync(libPackageJsonPath, JSON.stringify(libPackageJson, null, 2) + '\n');
fs.writeFileSync(libPackageLockJsonPath, JSON.stringify(libPackageLockJson, null, 2) + '\n');
