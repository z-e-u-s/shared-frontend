branchname=$1;
if [ -n "$branchname" ]; then
    echo "Updating Versions in package.json and package-lock.json"
    node scripts/update-versions.js

    echo "Checkout new branch ..."
    git checkout -b feature/$branchname
else
    echo "Please specify a branchname: E.g. npm run new-feature my-super-feature"
fi
