/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ['./src/**/*.{html,scss,ts}', './projects/shared/src/**/*.{html,ts}'],
  theme: {
    extend: {
      keyframes: {
        wiggle: {
          '0%, 100%': { transform: 'rotate(-3deg)' },
          '50%': { transform: 'rotate(3deg)' },
        },
        'fade-in-down': {
          '0%': {
            opacity: '0',
            transform: 'translateY(-10px)',
          },
          '100%': {
            opacity: '1',
            transform: 'translateY(0)',
          },
        },
        'fade-out-down': {
          from: {
            opacity: '1',
            transform: 'translateY(0px)',
          },
          to: {
            opacity: '0',
            transform: 'translateY(10px)',
          },
        },
        'fade-in-up': {
          '0%': {
            opacity: '0',
            transform: 'translateY(10px)',
          },
          '100%': {
            opacity: '1',
            transform: 'translateY(0)',
          },
        },
        'fade-out-up': {
          from: {
            opacity: '1',
            transform: 'translateY(0px)',
          },
          to: {
            opacity: '0',
            transform: 'translateY(10px)',
          },
        },
      },
      animation: {
        wiggle: 'wiggle 1s ease-in-out infinite',
        'fade-in-down': 'fade-in-down 0.3s ease-out',
        'fade-out-down': 'fade-out-down 0.3s ease-out',
        'fade-in-up': 'fade-in-up 0.3s ease-out',
        'fade-out-up': 'fade-out-up 0.3s ease-out',
      },
      boxShadow: {
        custom: '0px 0px 50px 0px rgb(82 63 105 / 15%)',
      },
      colors: {
        primary: {
          50: '#effaf1',
          100: '#d8f3db',
          200: '#b4e6bd',
          300: '#83d296',
          400: '#69c281',
          500: '#2d9c4e',
          600: '#1e7d3d',
          700: '#186432',
          800: '#155029',
          900: '#124223',
          950: '#092514',
        },
        night: {
          50: '#e4e4eb',
          100: '#bbbace',
          200: '#8f8ead',
          300: '#66658c',
          400: '#4b4777',
          500: '#302a62',
          600: '#2b245b',
          700: '#241c51',
          800: '#1c1445',
          900: '#130030',
        },
        accent: {
          // Lightest (for backgrounds or light elements):
          50: '#FFF4E6', // a very light almost white orange
          // Light (for less emphasized elements):
          100: '#FFE0B3', // a light peach
          200: '#FFCC80', // a softer, pastel orange
          // Medium (for buttons, icons, and active elements):
          300: '#FFB74D', // a bright but not overpowering orange
          400: '#FFA726', // a vivid orange
          // Regular (for action buttons and important highlights):
          500: '#FF9800', // a strong, attention-grabbing orange
          // Dark (for smaller text, icons, and less emphasized elements):
          600: '#FB8C00', // a slightly deeper orange
          700: '#F57C00', // a darker shade for good contrast on light backgrounds)
          // Darker (for strong accents in minimal amounts):
          800: '#EF6C00', // a rich, deep orange
          900: '#E65100', // a very dark, almost burnt orange
          // Darkest (sparingly for ultra important elements or accents):
          950: '#BF360C', // deep, earthy reddish-orange
          A100: '#BF360C',
          A200: '#BF360C',
          A400: '#BF360C',
          A700: '#BF360C',
        },
      },
    },
    fontFamily: {
      poppins: ['Poppins', 'system-ui', 'sans-serif'],
      nunito: ['Nunito Sans', 'sans-serif'],
      mono: ['"JetBrains Mono"', 'monospace'],
    },
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
    },
  },
  variants: {
    extend: {},
    scrollbar: ['dark', 'rounded'],
  },
  plugins: [
    require('@tailwindcss/forms')({
      strategy: 'class', // only generate classes
    }),
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
    require('tailwind-scrollbar'),
  ],
};
