export const environment = {
  production: true,
  mockRequests: false,
  configFile: 'assets/config/keycloak/config.json',
};
