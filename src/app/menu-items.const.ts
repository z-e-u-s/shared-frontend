import { MenuItem } from '../../projects/shared/src/lib/modules/layout/models/menu-item.model'; // eslint-disable-line import/no-relative-packages

export const MENU_ITEMS: MenuItem[] = [
  {
    group: 'Hauptmenü',
    separator: false,
    children: [
      {
        icon: 'assets/icons/tablericons/home.svg',
        label: 'Dashboard',
        route: '/dashboard',
        userRoles: [],
      },
      {
        icon: 'assets/icons/tablericons/folders.svg',
        label: 'Releases',
        route: '/releases',
        userRoles: [],
      },
      {
        icon: 'assets/icons/tablericons/tools.svg',
        label: 'Administration',
        route: '/administration',
        userRoles: [],
      },
    ],
  },
];
