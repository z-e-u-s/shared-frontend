import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { KeycloakAngularModule } from 'keycloak-angular';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { APP_INFO, SidenavWrapperComponent } from 'projects/shared/src/public-api';
import { TranslocoCoreModule } from './transloco/transloco.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  imports: [
    AngularSvgIconModule.forRoot(),
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslocoCoreModule,
    KeycloakAngularModule,
    DashboardComponent,
    SidenavWrapperComponent,
  ],
  providers: [
    {
      provide: APP_INFO,
      useValue: {
        appName: 'Shared Frontend',
        appShortName: 'SF',
        isProduction: environment.production,
      },
    },
    provideHttpClient(withInterceptorsFromDi()),
  ],
})
export class AppModule {}
