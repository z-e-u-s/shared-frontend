import { Component } from '@angular/core';
import { AppType } from 'projects/shared/src/lib/enum/app-type.enum';
import { MenuItem } from 'projects/shared/src/lib/modules/layout/models/menu-item.model';
import { VersionInfo } from 'projects/shared/src/lib/modules/layout/models/version-info.interface';
import { Observable, of } from 'rxjs';
import { MENU_ITEMS } from './menu-items.const';

/**
 * This is the root component of the Angular app
 *
 * - menu: contains items for a side navigation menu.
 * - versionInfo$: represents the meta-information data.
 * - frontendCommitHash: represents the current Git commit hash of the frontend codebase.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  menuItems: MenuItem[] = MENU_ITEMS;
  frontendCommitHash: string = '789101';

  isAuthDisabled: boolean = true;
  userProfile = {
    id: 'abcd-1234',
    username: 'test-user',
    email: 'test@test.de',
    firstName: 'Test',
    lastName: 'User',
    enabled: true,
    emailVerified: true,
  };
  protected readonly AppType = AppType;
  versionInfo$: Observable<VersionInfo> = of({
    backendVersion: '0.0.0',
    frontendVersion: '0.0.0',
    sharedFrontendVersion: '0.0.0',
    logprepVersion: '1.0.0',
    backendCommitHash: '123456',
    frontendCommitHash: '789101',
  });
}
