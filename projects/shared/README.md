# Shared

This project supports Angular version 18.x.

To run the application locally, you need to:

- setup Node.js (min 18.19.1) (https://nodejs.org/en/download), then
- install Angular CLI with the following command: `npm install -g @angular/cli`

## Install Project dependencies

`npm install`
