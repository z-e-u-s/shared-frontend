export type Theme = 'light' | 'dark';
export type Scheme = 'light' | 'dark';
