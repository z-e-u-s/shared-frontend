export interface BackendMetaInfo {
  lastCommit: string;
  version: string;
  logprepVersion?: string;
}
