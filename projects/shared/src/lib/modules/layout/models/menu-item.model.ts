import { ResourceItem } from '../../../models/resource-item.interface';

export interface MenuItem extends ResourceItem {
  group: string;
  separator?: boolean;
  selected?: boolean;
  active?: boolean;
  children: SubMenuItem[];
}

export interface SubMenuItem extends ResourceItem {
  icon?: string;
  label?: string;
  route?: string | null;
  expanded?: boolean;
  active?: boolean;
  children?: SubMenuItem[];
}
