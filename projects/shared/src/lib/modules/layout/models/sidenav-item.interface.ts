import { ResourceItem } from '../../../models/resource-item.interface';

export interface SidenavItem extends ResourceItem {
  routerLink: string;
  icon: string;
  title: string;
}
