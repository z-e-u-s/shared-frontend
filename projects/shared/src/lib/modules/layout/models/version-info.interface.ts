export interface VersionInfo {
  backendVersion: string;
  frontendVersion: string;
  sharedFrontendVersion: string;
  logprepVersion?: string;
  frontendCommitHash: string;
  backendCommitHash: string;
}
