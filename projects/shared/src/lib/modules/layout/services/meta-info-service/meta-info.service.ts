import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, of } from 'rxjs';
import { HttpEntityResponse } from '../../../../models/http-entity-response.interface';
import { BackendMetaInfo } from '../../models/backend-meta-info.interface';

@Injectable({ providedIn: 'root' })
export class MetaInfoService {
  httpClient = inject(HttpClient);

  getMetaInfo(metaInfoUrl: string): Observable<BackendMetaInfo> {
    return this.httpClient.get<HttpEntityResponse<BackendMetaInfo>>(metaInfoUrl).pipe(
      map(({ data }) => data),
      catchError(() => {
        const demoData: BackendMetaInfo = {
          lastCommit: 'N/A',
          version: 'N/A',
          logprepVersion: 'N/A',
        };
        return of(demoData);
      })
    );
  }
}
