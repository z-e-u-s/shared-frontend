import { DestroyRef, inject, Injectable, signal } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MenuItem } from '../../models/menu-item.model';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  destroyRef = inject(DestroyRef);
  private router = inject(Router);
  private showSidebarSignal = signal(true);
  private menuItemsSignal = signal<MenuItem[]>([]);
  private showMobileMenuSignal = signal<boolean>(false);

  showSidebar = this.showSidebarSignal.asReadonly();
  menuItems = this.menuItemsSignal.asReadonly();
  showMobileMenu = this.showMobileMenuSignal.asReadonly();

  setShowSidebar(newSidebarState: boolean): void {
    this.showSidebarSignal.set(newSidebarState);
  }

  toggleSidebar() {
    this.setShowSidebar(!this.showSidebarSignal());
  }

  toggleMobileMenu() {
    this.setShowMobileMenu(!this.showMobileMenuSignal());
  }

  setMenuItems(menuItems: MenuItem[]) {
    this.menuItemsSignal.set(menuItems);
    this.updateActiveState(this.menuItemsSignal());
  }

  setShowMobileMenu(newMobileMenuState: boolean): void {
    this.showMobileMenuSignal.set(newMobileMenuState);
  }

  /**
   * The following method updates the active state of the menuItems items based on the current route.
   * @param menuItems
   * @private
   */
  private updateActiveState(menuItems: MenuItem[]): void {
    this.router.events.pipe(takeUntilDestroyed(this.destroyRef)).subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Expand menuItems base on active route
        menuItems.forEach((menuItem) => {
          let activeGroup = false;

          menuItem.children.forEach((subMenu) => {
            const active = this.isActive(subMenu.route);
            subMenu.active = active; // eslint-disable-line no-param-reassign
            if (active) activeGroup = true;
          });
          menuItem.active = activeGroup; // eslint-disable-line no-param-reassign
        });
      }
    });
  }

  private isActive(instruction: any): boolean {
    return this.router.isActive(this.router.createUrlTree([instruction]), {
      paths: 'subset',
      queryParams: 'subset',
      fragment: 'ignored',
      matrixParams: 'ignored',
    });
  }
}
