import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockProvider } from 'ng-mocks';
import { provideAngularSvgIcon } from 'angular-svg-icon';
import { provideHttpClient } from '@angular/common/http';
import { getTranslocoTestingModule } from '../../../../transloco/transloco-testing.module';
import { MockSvgIconComponent } from '../../../../../test-data/svg-icon-component';
import { ThemeTogglerComponent } from './theme-toggler.component';
import { ThemeService } from '../../../../services/theme-service/theme.service';

describe('ThemeTogglerComponent', () => {
  let component: ThemeTogglerComponent;
  let fixture: ComponentFixture<ThemeTogglerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ThemeTogglerComponent, getTranslocoTestingModule(), MockSvgIconComponent],
      providers: [MockProvider(ThemeService), provideHttpClient(), provideAngularSvgIcon()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeTogglerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
