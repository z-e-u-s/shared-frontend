import { ChangeDetectionStrategy, Component, EventEmitter, inject, Input, Output } from '@angular/core';

import { SvgIconComponent } from 'angular-svg-icon';
import { TranslocoCoreModule } from '../../../../transloco/transloco.module';
import { ThemeService } from '../../../../services/theme-service/theme.service';
import { Theme } from '../../models/scheme.types';

@Component({
  selector: 'lib-theme-toggler',
  templateUrl: './theme-toggler.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [TranslocoCoreModule, SvgIconComponent],
})
export class ThemeTogglerComponent {
  public themeService = inject(ThemeService);
  @Input() showTooltip: boolean = false;
  @Output() themeChanged = new EventEmitter<void>();

  toggleTheme() {
    const theme: Theme = !this.themeService.isDark ? 'dark' : 'light';
    this.themeService.theme = theme;
    this.themeService.refreshUI(theme);
    this.themeChanged.emit();
  }
}
