import { ChangeDetectionStrategy, Component, Input, OnInit, inject, DestroyRef } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { TranslocoPipe } from '@jsverse/transloco';
import { MatIcon } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { VersionInfo } from '../../models/version-info.interface';
import { ThemeService } from '../../../../services/theme-service/theme.service';
import { MenuItem } from '../../models/menu-item.model';
import { AppType } from '../../../../enum/app-type.enum';
import { ResponsiveHelperComponent } from '../../../../components/responsive-helper/responsive-helper.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { LoadingProgressComponent } from '../../../../components/loading-progress/loading-progress.component';

@Component({
  selector: 'lib-sidenav-wrapper',
  templateUrl: './sidenav-wrapper.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    SidebarComponent,
    NavbarComponent,
    MatIcon,
    LoadingProgressComponent,
    RouterOutlet,
    ResponsiveHelperComponent,
    TranslocoPipe,
  ],
})
export class SidenavWrapperComponent implements OnInit {
  private destroyRef = inject(DestroyRef);
  private router = inject(Router);
  readonly keycloakService = inject(KeycloakService);
  public themeService = inject(ThemeService);

  @Input({ required: true }) menuItems!: MenuItem[];
  @Input() frontendCommitHash!: string;
  @Input() versionInfo: VersionInfo | null | undefined;
  @Input() appType!: AppType;
  @Input() showScreenSizeHelper: boolean = false;
  @Input() showLogprepVersion: boolean = true;
  @Input() isAuthDisabled: boolean = false;
  @Input() userProfile: KeycloakProfile | undefined;

  private mainContent: HTMLElement | null = null;

  /**
   * In this lifecycle hook, we get the main content element.
   * We also refresh the UI depeneding on the current theme (scheme),
   * and subscribe to the router events to scroll to the top of the page when the route changes.
   */
  async ngOnInit(): Promise<void> {
    const scheme = this.themeService.theme === 'dark' ? 'dark' : 'light';
    this.themeService.refreshUI(scheme);

    this.mainContent = document.getElementById('main-content');

    this.router.events.pipe(takeUntilDestroyed(this.destroyRef)).subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.mainContent) {
          this.mainContent!.scrollTop = 0;
        }
      }
    });
  }

  /**
   * This method is called when the user clicks on the logout button.
   */
  async logout(): Promise<void> {
    await this.keycloakService.logout();
  }
}
