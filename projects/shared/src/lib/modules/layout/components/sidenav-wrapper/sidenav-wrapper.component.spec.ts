import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { KeycloakService } from 'keycloak-angular';
import { MockComponent, MockModule, MockProviders } from 'ng-mocks';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ResponsiveHelperComponent } from '../../../../components/responsive-helper/responsive-helper.component';
import { ThemeService } from '../../../../services/theme-service/theme.service';
import { getTranslocoTestingModule } from '../../../../transloco/transloco-testing.module';
import { NavbarComponent } from '../navbar/navbar.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { SidenavWrapperComponent } from './sidenav-wrapper.component';
import { LoadingProgressComponent } from '../../../../components/loading-progress/loading-progress.component';

describe('SidenavWrapperComponent', () => {
  let component: SidenavWrapperComponent;
  let fixture: ComponentFixture<SidenavWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        SidenavWrapperComponent,
        CommonModule,
        RouterModule,
        getTranslocoTestingModule(),
        MockComponent(SidebarComponent),
        MockComponent(ResponsiveHelperComponent),
        MockComponent(LoadingProgressComponent),
        MockComponent(NavbarComponent),
        MockModule(MatIconModule),
      ],
      providers: [MockProviders(KeycloakService, ThemeService)],
    }).compileComponents();

    fixture = TestBed.createComponent(SidenavWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('should not translate translations without correct scope "shared"', () => {
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('h4').textContent).toEqual('TOOLBAR.TITLE');
  });
});
