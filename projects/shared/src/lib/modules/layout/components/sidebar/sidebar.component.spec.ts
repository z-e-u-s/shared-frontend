import { signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { SvgIconComponent, SvgIconRegistryService } from 'angular-svg-icon';
import { MockComponent, MockProvider } from 'ng-mocks';
import { APP_INFO } from '../../../../tokens/app-info.token';
import { getTranslocoTestingModule } from '../../../../transloco/transloco-testing.module';
import { MenuService } from '../../services/menu-service/menu.service';
import { ThemeTogglerComponent } from '../theme-toggler/theme-toggler.component';
import { SidebarMenuComponent } from './sidebar-menu/sidebar-menu.component';
import { SidebarComponent } from './sidebar.component';

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  let menuServiceMock: any;
  let router: Router;

  beforeEach(async () => {
    menuServiceMock = {
      showSidebar: signal<boolean>(true),
      toggleSidebar: jest.fn(),
    };

    const routes = [
      { path: '.', component: SidebarComponent },
      { path: 'faq', component: SidebarComponent },
    ];

    await TestBed.configureTestingModule({
      imports: [
        SidebarComponent,
        RouterModule.forRoot(routes),
        getTranslocoTestingModule(),
        MockComponent(SidebarMenuComponent),
        MockComponent(ThemeTogglerComponent),
        MockComponent(SvgIconComponent),
      ],
      providers: [
        MockProvider(SvgIconRegistryService),
        MockProvider(ActivatedRoute),
        { provide: MenuService, useValue: menuServiceMock },
        {
          provide: APP_INFO,
          useValue: { appName: 'Field Dictionary Application', appShortName: 'FDA', version: '0.6.9' },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('toggleSidebar button', () => {
    it('should be rendered', () => {
      const toggleSidebarButton = fixture.nativeElement.querySelector('#toggle-sidebar-button');
      expect(toggleSidebarButton).toBeTruthy();
    });

    it('should call toggleSidebar() when clicked', () => {
      jest.spyOn(component, 'toggleSidebar');
      const toggleSidebarButton = fixture.nativeElement.querySelector('#toggle-sidebar-button');
      toggleSidebarButton.click();
      expect(component.toggleSidebar).toHaveBeenCalledTimes(1);
    });

    it('should have a tooltip when menuService.showSidebar is false', () => {
      menuServiceMock.showSidebar.set(false);
      fixture.detectChanges();
      const tooltip = fixture.debugElement.query(By.css('#toggle-sidebar-button > div > span'));
      expect(tooltip).toBeTruthy();
      // could add test for tooltip text (transloco pipe)
    });
  });

  describe('FAQ link', () => {
    it('should be rendered', () => {
      const faqLink = fixture.nativeElement.querySelector('#faq-link');
      expect(faqLink).toBeTruthy();
    });

    it('should have the correct href attribute and navigate to /faq when clicked', async () => {
      const faqLink = fixture.nativeElement.querySelector('#faq-link');
      expect(faqLink).toBeTruthy();
      expect(faqLink.getAttribute('href')).toBe('/faq');
      faqLink.click();
      await fixture.whenStable();
      expect(router.url).toBe('/faq');
    });

    it('should have a label when menuService.showSidebar is true, but no tooltip', () => {
      const label = fixture.nativeElement.querySelector('#faq-link > span');
      const tooltip = fixture.debugElement.query(By.css('#faq-link > div > span'));
      expect(label).toBeTruthy();
      expect(tooltip).toBeFalsy();
    });

    it('should have a tooltip when menuService.showSidebar is false, but no label', () => {
      menuServiceMock.showSidebar.set(false);
      fixture.detectChanges();
      const label = fixture.nativeElement.querySelector('#faq-link > span');
      const tooltip = fixture.debugElement.query(By.css('#faq-link > div > span'));
      expect(label).toBeFalsy();
      expect(tooltip).toBeTruthy();
    });
  });

  describe('Logout button', () => {
    it('should be rendered', () => {
      const logoutButton = fixture.nativeElement.querySelector('#logout-button');
      expect(logoutButton).toBeTruthy();
    });

    it('should emit logout event when clicked', () => {
      jest.spyOn(component.logout, 'emit');
      const logoutButton = fixture.nativeElement.querySelector('#logout-button');
      logoutButton.click();
      expect(component.logout.emit).toHaveBeenCalledTimes(1);
    });

    it('should only have a label when menuService.showSidebar is true', () => {
      const label = fixture.nativeElement.querySelector('#logout-button > span');
      const tooltip = fixture.debugElement.query(By.css('#logout-button > div > span'));
      expect(label).toBeTruthy();
      expect(tooltip).toBeFalsy();
    });

    it('should only have a tooltip when menuService.showSidebar is false', () => {
      menuServiceMock.showSidebar.set(false);
      fixture.detectChanges();
      const label = fixture.nativeElement.querySelector('#logout-button > span');
      const tooltip = fixture.debugElement.query(By.css('#logout-button > div > span'));
      expect(label).toBeFalsy();
      expect(tooltip).toBeTruthy();
    });
  });

  describe('toggleSidebar', () => {
    it('should call menuService.toggleSidebar()', () => {
      component.toggleSidebar();
      expect(menuServiceMock.toggleSidebar).toHaveBeenCalledTimes(1);
    });
  });
});
