import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { TranslocoModule } from '@jsverse/transloco';
import { SvgIconComponent } from 'angular-svg-icon';
import { AppType } from '../../../../enum/app-type.enum';
import { APP_INFO } from '../../../../tokens/app-info.token';
import { MenuService } from '../../services/menu-service/menu.service';
import { ThemeTogglerComponent } from '../theme-toggler/theme-toggler.component';
import { SidebarMenuComponent } from './sidebar-menu/sidebar-menu.component';

@Component({
  selector: 'lib-sidebar',
  templateUrl: './sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    RouterLinkActive,
    TranslocoModule,
    SvgIconComponent,
    MatTooltipModule,
    SidebarMenuComponent,
    ThemeTogglerComponent,
  ],
})
export class SidebarComponent {
  protected readonly appInfo = inject(APP_INFO);
  protected readonly menuService = inject(MenuService);

  @Input() appType!: AppType;
  @Output() logout = new EventEmitter<void>();

  toggleSidebar() {
    this.menuService.toggleSidebar();
  }
}
