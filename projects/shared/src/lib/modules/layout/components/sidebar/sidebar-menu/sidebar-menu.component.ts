import { NgClass } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { TranslocoCoreModule } from '../../../../../transloco/transloco.module';
import { MenuService } from '../../../services/menu-service/menu.service';

@Component({
  selector: 'lib-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgClass, AngularSvgIconModule, RouterLink, RouterLinkActive, TranslocoCoreModule],
})
export class SidebarMenuComponent {
  protected readonly menuService = inject(MenuService);
}
