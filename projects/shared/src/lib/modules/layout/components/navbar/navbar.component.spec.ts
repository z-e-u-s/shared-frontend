import { signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SvgIconComponent } from 'angular-svg-icon';
import { KeycloakProfile } from 'keycloak-js';
import { MockComponent } from 'ng-mocks';
import { getTranslocoTestingModule } from '../../../../transloco/transloco-testing.module';
import { BreadcrumbComponent } from '../../../../components/breadcrumb/breadcrumb.component';
import { APP_INFO } from '../../../../tokens/app-info.token';
import { MenuItem } from '../../models/menu-item.model';
import { MenuService } from '../../services/menu-service/menu.service';
import { NavbarMobileComponent } from './navbar-mobile/navbar-mobile.component';
import { NavbarComponent } from './navbar.component';
import { ProfileMenuComponent } from './profile-menu/profile-menu.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let menuServiceMock: any;
  let appInfoMock: any;

  beforeEach(async () => {
    menuServiceMock = {
      showMobileMenu: signal<boolean>(false),
      showSidebar: signal<boolean>(false),
      setMenuItems: jest.fn(),
      toggleMobileMenu: jest.fn(),
      setShowMobileMenu: jest.fn(),
    };
    appInfoMock = { name: 'Test App', version: '1.0.0' };

    await TestBed.configureTestingModule({
      imports: [
        NavbarComponent,
        ProfileMenuComponent,
        MockComponent(BreadcrumbComponent),
        MockComponent(NavbarMobileComponent),
        MockComponent(SvgIconComponent),
        MatTooltipModule,
        getTranslocoTestingModule(),
      ],
      providers: [
        { provide: MenuService, useValue: menuServiceMock },
        { provide: APP_INFO, useValue: appInfoMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    menuServiceMock = TestBed.inject(MenuService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set menu items on changes', () => {
    const menuItems: MenuItem[] = [
      { group: 'Home', children: [{ route: 'dashboard', label: 'Dashboard', icon: 'dashboard.svg' }] },
    ];
    component.menuItems = menuItems;
    component.ngOnChanges({
      menuItems: {
        currentValue: menuItems,
        previousValue: null,
        firstChange: true,
        isFirstChange: () => true,
      },
    });
    expect(menuServiceMock.setMenuItems).toHaveBeenCalledWith(menuItems);
  });

  it('should toggle mobile menu', () => {
    component.toggleMobileMenu();
    expect(menuServiceMock.toggleMobileMenu).toHaveBeenCalledTimes(1);
  });

  it('should close mobile menu', () => {
    component.closeMobileMenu();
    expect(menuServiceMock.setShowMobileMenu).toHaveBeenCalledWith(false);
  });

  it('should emit logout event', () => {
    jest.spyOn(component.logout, 'emit');
    component.logout.emit();
    expect(component.logout.emit).toHaveBeenCalledTimes(1);
  });

  it('should render user-menu-button', () => {
    const userProfile: KeycloakProfile = { username: 'testuser' };
    component.userProfile = userProfile;
    fixture.detectChanges();
    const userMenuButton = fixture.nativeElement.querySelector('#user-menu-button');
    expect(userMenuButton).toBeTruthy();
  });
});
