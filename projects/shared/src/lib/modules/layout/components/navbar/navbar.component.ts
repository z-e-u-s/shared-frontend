import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  inject,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { SvgIconComponent } from 'angular-svg-icon';
import { KeycloakProfile } from 'keycloak-js';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AppType } from '../../../../enum/app-type.enum';
import { APP_INFO } from '../../../../tokens/app-info.token';
import { TranslocoCoreModule } from '../../../../transloco/transloco.module';
import { MenuItem } from '../../models/menu-item.model';
import { VersionInfo } from '../../models/version-info.interface';
import { MenuService } from '../../services/menu-service/menu.service';
import { NavbarMobileComponent } from './navbar-mobile/navbar-mobile.component';
import { ProfileMenuComponent } from './profile-menu/profile-menu.component';
import { BreadcrumbComponent } from '../../../../components/breadcrumb/breadcrumb.component';

@Component({
  selector: 'lib-navbar',
  templateUrl: './navbar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    SvgIconComponent,
    ProfileMenuComponent,
    NavbarMobileComponent,
    BreadcrumbComponent,
    TranslocoCoreModule,
    MatTooltipModule,
  ],
})
export class NavbarComponent implements OnChanges {
  protected readonly appInfo = inject(APP_INFO);
  protected readonly menuService = inject(MenuService);

  @Input({ required: true }) menuItems!: MenuItem[];
  @Input() appType!: AppType;
  @Input() userProfile: KeycloakProfile | undefined;
  @Input() versionInfo: VersionInfo | null | undefined;
  @Output() logout = new EventEmitter<void>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['menuItems']?.currentValue) {
      this.menuService.setMenuItems(this.menuItems);
    }
  }

  toggleMobileMenu(): void {
    this.menuService.toggleMobileMenu();
  }

  closeMobileMenu(): void {
    this.menuService.setShowMobileMenu(false);
  }
}
