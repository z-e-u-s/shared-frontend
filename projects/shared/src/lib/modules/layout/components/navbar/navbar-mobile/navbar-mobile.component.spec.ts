import { signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActivatedRoute, Router, RouterLinkActive, RouterModule } from '@angular/router';
import { SvgIconComponent } from 'angular-svg-icon';
import { MockComponent, MockProvider } from 'ng-mocks';
import { MOCK_MENU_ITEMS } from '../../../../../../test-data/menu-items';
import { getTranslocoTestingModule } from '../../../../../transloco/transloco-testing.module';
import { ClickInsideOutsideDirective } from '../../../../../directives/click-inside-outside.directive';
import { APP_INFO } from '../../../../../tokens/app-info.token';
import { MenuItem } from '../../../models/menu-item.model';
import { MenuService } from '../../../services/menu-service/menu.service';
import { ThemeTogglerComponent } from '../../theme-toggler/theme-toggler.component';
import { NavbarMobileComponent } from './navbar-mobile.component';

describe('NavbarMobileComponent', () => {
  let component: NavbarMobileComponent;
  let fixture: ComponentFixture<NavbarMobileComponent>;
  let menuServiceMock: any;
  let router: Router;
  let routes: any;

  beforeEach(async () => {
    menuServiceMock = {
      menuItems: signal<MenuItem[]>(MOCK_MENU_ITEMS),
      showSidebar: signal<boolean>(true),
      showMobileMenu: signal<boolean>(true),
      toggleSidebar: jest.fn(),
    };

    routes = [
      { path: 'faq', component: NavbarMobileComponent },
      { path: 'dashboard', component: NavbarMobileComponent },
      { path: 'releases', component: NavbarMobileComponent },
      { path: 'administration', component: NavbarMobileComponent },
    ];

    await TestBed.configureTestingModule({
      imports: [
        NavbarMobileComponent,
        getTranslocoTestingModule(),
        ClickInsideOutsideDirective,
        MatTooltipModule,
        RouterModule.forRoot(routes),
        RouterLinkActive,
        ThemeTogglerComponent,
        MockComponent(SvgIconComponent),
      ],
      providers: [
        { provide: MenuService, useValue: menuServiceMock },
        {
          provide: APP_INFO,
          useValue: { appName: 'Field Dictionary Application', appShortName: 'FDA', version: '0.6.9' },
        },
        MockProvider(ActivatedRoute),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NavbarMobileComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the short app name', () => {
    const appName = fixture.nativeElement.querySelector('b');
    expect(appName).toBeTruthy();
    expect(appName.textContent).toContain('FDA');
  });

  describe('Menu items', () => {
    it('should render a link for an item, with the correct router-link attribute', () => {
      const itemLink = fixture.nativeElement.querySelector('#item-link-0') as HTMLAnchorElement;
      expect(itemLink).toBeTruthy();
      expect(itemLink.getAttribute('ng-reflect-router-link')).toBe('dashboard');
    });

    it('should render a link for a sub-item, with the correct router-link attribute', () => {
      const subItemLink = fixture.nativeElement.querySelector('#sub-item-link-0') as HTMLAnchorElement;
      expect(subItemLink).toBeTruthy();
      expect(subItemLink.getAttribute('ng-reflect-router-link')).toBe('releases/new');
    });
  });

  describe('FAQ link', () => {
    it('should be rendered', () => {
      const faqLink = fixture.nativeElement.querySelector('#faq-link');
      expect(faqLink).toBeTruthy();
    });

    it('should have the correct href attribute and navigate to /faq when clicked', async () => {
      const faqLink = fixture.nativeElement.querySelector('#faq-link');
      expect(faqLink).toBeTruthy();
      expect(faqLink.getAttribute('href')).toBe('/faq');
      faqLink.click();
      await fixture.whenStable();
      expect(router.url).toBe('/faq');
    });
  });
});
