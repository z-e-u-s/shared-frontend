import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, inject, Output } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterLinkActive, RouterModule } from '@angular/router';
import { SvgIconComponent } from 'angular-svg-icon';
import { ClickInsideOutsideDirective } from '../../../../../directives/click-inside-outside.directive';
import { APP_INFO } from '../../../../../tokens/app-info.token';
import { TranslocoCoreModule } from '../../../../../transloco/transloco.module';
import { MenuService } from '../../../services/menu-service/menu.service';
import { ThemeTogglerComponent } from '../../theme-toggler/theme-toggler.component';

@Component({
  selector: 'lib-navbar-mobile',
  templateUrl: './navbar-mobile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    RouterLinkActive,
    SvgIconComponent,
    TranslocoCoreModule,
    ThemeTogglerComponent,
    MatTooltipModule,
    ClickInsideOutsideDirective,
  ],
})
export class NavbarMobileComponent {
  protected readonly appInfo = inject(APP_INFO);
  protected readonly menuService = inject(MenuService);

  @Output() closeMobileMenu = new EventEmitter<void>();
}
