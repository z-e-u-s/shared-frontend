import { NgClass } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { TranslocoModule } from '@jsverse/transloco';
import { KeycloakProfile } from 'keycloak-js';
import { NgxJdenticonModule } from 'ngx-jdenticon';
import { ClickInsideOutsideDirective } from '../../../../../directives/click-inside-outside.directive';
import { VersionInfo } from '../../../models/version-info.interface';

@Component({
  selector: 'lib-profile-menu',
  templateUrl: './profile-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [ClickInsideOutsideDirective, NgClass, NgxJdenticonModule, MatMenuModule, MatIconModule, TranslocoModule],
})
export class ProfileMenuComponent {
  @Input() userProfile: KeycloakProfile | undefined;
  @Input() isAuthDisabled: boolean = false;
  @Input() versionInfo: VersionInfo | null | undefined;
  @Input() showLogprepVersion: boolean = true;
  @Output() logout = new EventEmitter<void>();

  isMenuOpen = false;

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }
}
