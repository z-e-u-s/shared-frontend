/**
 * Selectable application names. E. g., they are used to display the application name in the sidebar.
 */
export enum AppType {
  FDA = 'FDA',
  UCL = 'UCL',
}
