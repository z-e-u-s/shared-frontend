import { InjectionToken } from '@angular/core';
import { AppInfo } from '../models/app-info.interface';

export const APP_INFO = new InjectionToken<AppInfo>('APP_INFO');
