import { provideTransloco, Translation, TRANSLOCO_SCOPE, TranslocoModule, TranslocoService } from '@jsverse/transloco';
import { APP_INITIALIZER, NgModule } from '@angular/core';

// DO NOT EXPOSE THIS TRANSLOCO MODULE
// ONLY FOR THIS SHARED MODULE

export const loader: { [key: string]: () => Promise<any> } = {};
const languages = ['de'];

languages.forEach((language) => {
  loader[language] = () => import(`../i18n/${language}.json`);
});

@NgModule({
  exports: [TranslocoModule],
  providers: [
    provideTransloco({
      config: {
        availableLangs: [{ id: 'de', label: 'Deutsch' }],
        defaultLang: 'de',
        fallbackLang: 'de',
        reRenderOnLangChange: false,
      },
    }),
    {
      provide: TRANSLOCO_SCOPE,
      useValue: { scope: 'shared', loader },
    },
    {
      // Preload the default language before the app starts to prevent empty/jumping content
      provide: APP_INITIALIZER,
      deps: [TranslocoService],
      useFactory:
        (translocoService: TranslocoService): any =>
        (): Promise<Translation | undefined> => {
          const defaultLang = translocoService.getDefaultLang();
          translocoService.setActiveLang(defaultLang);
          return translocoService.load(defaultLang).toPromise();
        },
      multi: true,
    },
  ],
})
export class TranslocoCoreModule {}
