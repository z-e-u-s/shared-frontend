import { TranslocoTestingModule, TranslocoTestingOptions } from '@jsverse/transloco';
import de from '../i18n/de.json';

// DO NOT EXPOSE THIS TRANSLOCO TESTING MODULE
// ONLY FOR TESTS IN THIS SHARED MODULE
export function getTranslocoTestingModule(options: TranslocoTestingOptions = {}) {
  return TranslocoTestingModule.forRoot({
    langs: { 'shared/de': de },
    translocoConfig: {
      availableLangs: [{ id: 'de', label: 'Deutsch' }],
      defaultLang: 'de',
      fallbackLang: 'de',
      reRenderOnLangChange: false,
    },
    preloadLangs: true,
    ...options,
  });
}
