import { AbstractControl, FormControl } from '@angular/forms';
import { isValidJSON } from './is-valid-json.validator';

describe('isValidJSON', () => {
  let control: AbstractControl;

  beforeEach(() => {
    control = new FormControl();
  });

  describe('isValidJSON', () => {
    it('should return null if the control value is empty', () => {
      expect(isValidJSON()(control)).toBeNull();
    });

    it('should return null if the control value is a valid JSON object', () => {
      control.setValue('{"name": "John", "age": 30}');
      expect(isValidJSON()(control)).toBeNull();
    });

    it('should return validation error when control value has leading spaces in keys', () => {
      control.setValue('{" name": "John", "age": 30}');
      const result = isValidJSON()(control);
      expect(result).not.toBe(null);
      expect(result!['isNotJson']).toBe(control.value);
    });

    it('should return validation error when control value has trailing spaces in keys', () => {
      control.setValue('{"name ": "John", "age": 30}');
      const result = isValidJSON()(control);
      expect(result).not.toBe(null);
      expect(result!['isNotJson']).toBe(control.value);
    });

    it('should return validation error when control value has duplicate keys on same layer', () => {
      control.setValue('{"name": "John", "name": "Doe"}');
      const result = isValidJSON()(control);
      expect(result).not.toBe(null);
      expect(result!['isNotJson']).toBe(control.value);
    });

    it('should return null when control value has duplicate keys not on same layer', () => {
      control.setValue('{"name": {"name": "Doe"}}');
      expect(isValidJSON()(control)).toBeNull();
    });

    it('should return null when control key is no string', () => {
      control.setValue('{name: "Doe"}');
      const result = isValidJSON()(control);
      expect(result).not.toBe(null);
      expect(result!['isNotJson']).toBe(control.value);
    });

    it('should return null if the control value is a valid JSON object without leading/trailing spaces and no duplicate keys', () => {
      control.setValue('{"name": {"firstName": "John", "name": "Doe"}, "age": 30}');
      expect(isValidJSON()(control)).toBeNull();
    });
  });
});
