import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import * as YAML from 'js-yaml';
import { Observable, catchError, from, map } from 'rxjs';

/**
 * This function checks if a YAML string can be parsed as JSON.
 * @param yamlString
 * @returns Can the YAML string be parsed as JSON?
 */
export function canYamlBeParsed(yamlString: string): boolean {
  try {
    const json = YAML.load(yamlString, { schema: YAML.JSON_SCHEMA });
    return typeof json === 'object';
  } catch (error) {
    return false;
  }
}

/**
 * This function is a custom validator for Files with YAML strings.
 */
export function yamlFilesValidator(): AsyncValidatorFn {
  // eslint-disable-next-line prettier/prettier
  return (control: AbstractControl): Observable<null | { invalidYamlFiles: boolean }> => {
    const files = control.value as File[];
    if (
      !files ||
      files.length === 0 ||
      ![...files].some((file) => file.name.endsWith('.yml') || file.name.endsWith('.yaml'))
    ) {
      return from(Promise.resolve(null));
    }
    const fileValidations = [...files].map((file) => {
      return new Promise<boolean>((resolve) => {
        const reader = new FileReader();
        reader.onload = () => {
          if (canYamlBeParsed(reader.result as string)) {
            resolve(true);
          } else {
            resolve(false);
          }
        };
        reader.onerror = () => resolve(false);
        // emits .onload event
        reader.readAsText(file);
      });
    });
    return from(Promise.all(fileValidations)).pipe(
      map((results) => {
        const allValid = results.every((isValid) => isValid);
        return allValid ? null : { invalidYamlFiles: true };
      }),
      catchError(() => from(Promise.resolve({ invalidYamlFiles: true })))
    );
  };
}
