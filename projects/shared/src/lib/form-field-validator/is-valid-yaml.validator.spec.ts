import { AbstractControl, FormControl } from '@angular/forms';
import { from } from 'rxjs';
import { canYamlBeParsed, yamlFilesValidator } from './is-valid-yaml.validator';

describe('is-valid-YAML', () => {
  let control: AbstractControl;

  beforeEach(() => {
    control = new FormControl();
  });

  describe('canYamlBeParsed()', () => {
    it('should return true for valid YAML string', () => {
      const validYaml = `
        name: John Doe
        age: 30
        address:
          street: 123 Main St
          city: Anytown
      `;
      expect(canYamlBeParsed(validYaml)).toBe(true);
    });

    it('should return false for invalid YAML string', () => {
      const invalidYaml = `
        name: John Doe
        age: 30
        address:
          street: 123 Main St
          city: Anytown
          - invalid
      `;
      expect(canYamlBeParsed(invalidYaml)).toBe(false);
    });

    it('should return false for non-YAML string', () => {
      const nonYaml = 'This is not a YAML string';
      expect(canYamlBeParsed(nonYaml)).toBe(false);
    });

    it('should return false for empty string', () => {
      const emptyYaml = '';
      expect(canYamlBeParsed(emptyYaml)).toBe(false);
    });
  });

  describe('yamlFilesValidator()', () => {
    it('should return null if no files are provided', (done) => {
      control.setValue([]);
      const validator = yamlFilesValidator();
      from(validator(control)).subscribe((result: any) => {
        expect(result).toBeNull();
        done();
      });
    });

    it('should return null if files do not have .yaml or .yml extension', (done) => {
      const mockFile = new File(['content'], 'test.txt', { type: 'text/plain' });
      control.setValue([mockFile]);
      const validator = yamlFilesValidator();
      from(validator(control)).subscribe((result: any) => {
        expect(result).toBeNull();
        done();
      });
    });

    it('should return { invalidYamlFiles: true } if any file has invalid YAML content', (done) => {
      const invalidYamlContent = `
      test:
      description
        - invalid
      name:
      `;
      const mockFile = new File([invalidYamlContent], 'test.yaml', { type: 'text/yaml' });

      const reader = {
        result: invalidYamlContent,
        onload: null as any,
        onerror: null as any,
        readAsText: jest.fn().mockImplementation(function readAsText(this: FileReader) {
          this.onload!({ target: { result: invalidYamlContent } } as ProgressEvent<FileReader>);
        }),
      };

      jest.spyOn(window, 'FileReader').mockImplementation(() => reader as unknown as FileReader);

      control.setValue([mockFile]);
      const validator = yamlFilesValidator();
      from(validator(control)).subscribe((result: any) => {
        expect(result).toEqual({ invalidYamlFiles: true });
        done();
      });
    });

    it('should return null if all files have valid YAML content', (done) => {
      const validYamlContent = `
      key: value
      list:
        - item1
        - item2
      `;
      const mockFile = new File([validYamlContent], 'test.yaml', { type: 'text/yaml' });

      const reader = {
        result: validYamlContent,
        onload: null as any,
        onerror: null as any,
        readAsText: jest.fn().mockImplementation(function readAsText(this: FileReader) {
          this.onload!({ target: { result: validYamlContent } } as ProgressEvent<FileReader>);
        }),
      };

      jest.spyOn(window, 'FileReader').mockImplementation(() => reader as unknown as FileReader);

      control.setValue([mockFile]);
      const validator = yamlFilesValidator();
      from(validator(control)).subscribe((result: any) => {
        expect(result).toBeNull();
        done();
      });
    });

    it('should return { invalidYamlFiles: true } if file cannot be read', (done) => {
      const invalidYamlContent = `
      test:
      description
        - invalid
      name:
      `;
      const mockFile = new File([invalidYamlContent], 'test.yaml', { type: 'text/yaml' });

      const reader = {
        result: invalidYamlContent,
        onload: null as any,
        onerror: null as any,
        readAsText: jest.fn().mockImplementation(function readAsText(this: FileReader) {
          this.onerror!({ target: { result: invalidYamlContent } } as ProgressEvent<FileReader>);
        }),
      };

      jest.spyOn(window, 'FileReader').mockImplementation(() => reader as unknown as FileReader);

      control.setValue([mockFile]);
      const validator = yamlFilesValidator();
      from(validator(control)).subscribe((result: any) => {
        expect(result).toEqual({ invalidYamlFiles: true });
        done();
      });
    });
  });
});
