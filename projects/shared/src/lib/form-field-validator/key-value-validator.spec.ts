import { AbstractControl } from '@angular/forms';
import { keyValueValidator, validateKeyValueString } from './key-value-validator';

describe('keyValueValidator', () => {
  it('should return null for valid input', () => {
    const control: AbstractControl = { value: 'key:value' } as AbstractControl;
    const validatorFn = keyValueValidator();
    const result = validatorFn(control);
    expect(result).toBeNull();
  });

  it('should return an error object for invalid input', () => {
    const control: AbstractControl = { value: 'invalid-key-value' } as AbstractControl;
    const validatorFn = keyValueValidator();
    const result = validatorFn(control);
    expect(result).toEqual({ keyValueError: true });
  });

  it('should return null for null input', () => {
    const control: AbstractControl = { value: null } as AbstractControl;
    const validatorFn = keyValueValidator();
    const result = validatorFn(control);
    expect(result).toBeNull();
  });
});

describe('validateKeyValueString', () => {
  it('should return null for valid input', () => {
    const result = validateKeyValueString('key:value');
    expect(result).toBeNull();
  });

  it('should return an error object for invalid input', () => {
    const result = validateKeyValueString('invalid-key-value');
    expect(result).toEqual({ keyValueError: true });
  });

  it('should return null for null input', () => {
    const result = validateKeyValueString(null);
    expect(result).toBeNull();
  });
});
