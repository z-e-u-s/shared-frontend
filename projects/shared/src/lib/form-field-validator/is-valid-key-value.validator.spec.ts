import { AbstractControl, FormControl } from '@angular/forms';
import { isKeyValue } from './is-valid-key-value.validator';

describe('isKeyValue', () => {
  let control: AbstractControl;

  beforeEach(() => {
    control = new FormControl();
  });

  it('should return null if the control value is empty', () => {
    expect(isKeyValue()(control)).toBeNull();
  });

  it('should fail if the control value contains invalid data types', () => {
    const value = '{"name": "John", "age": 30}';
    control.setValue(value);
    expect(isKeyValue()(control)).toEqual({ isNotKeyValue: value });
  });

  it('should fail if the control value contains invalid data types 2', () => {
    const value = '{"name": "John", "age": "30", "family": ["Jane", 30]}';
    control.setValue(value);
    expect(isKeyValue()(control)).toEqual({ isNotKeyValue: value });
  });

  it('should fail if the control value contains invalid data types 3', () => {
    const value = '{"name": "John", "age": "30", "food": {"coco": 30}}';
    control.setValue(value);
    expect(isKeyValue()(control)).toEqual({ isNotKeyValue: value });
  });

  it('should fail if the control value contains invalid data types 4', () => {
    const value = '{"name": "John", "age": "30", "food": {"coco": ["water", 30]}}';
    control.setValue(value);
    expect(isKeyValue()(control)).toEqual({ isNotKeyValue: value });
  });

  it('should return null if the control value is a valid JSON object containing valid data types only', () => {
    control.setValue('{"name": "John", "age": "30"}');
    expect(isKeyValue()(control)).toBeNull();
  });

  it('should return null if the control value is a valid JSON object containing valid data types only 2', () => {
    control.setValue('{"name": "John", "age": "30", "family": ["Jane", "Jack"]}');
    expect(isKeyValue()(control)).toBeNull();
  });

  it('should return null if the control value is a valid JSON object containing valid data types only 3', () => {
    control.setValue('{"name": "John", "age": "30", "food": {"coco": "nut"}}');
    expect(isKeyValue()(control)).toBeNull();
  });

  it('should return null if the control value is a valid JSON object containing valid data types only 4', () => {
    control.setValue('{"name": "John", "age": "30", "food": {"coco": ["nut"]}}');
    expect(isKeyValue()(control)).toBeNull();
  });

  it('should return validation error when control value is not valid JSON', () => {
    control.setValue('{foo: "bar"}');

    const result = isKeyValue()(control);

    expect(result).not.toBe(null);
    // @ts-ignore
    expect(result.isNotKeyValue).toBe(control.value);
  });
});
