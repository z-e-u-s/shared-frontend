import { AbstractControl, AsyncValidatorFn, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Observable, catchError, from, map } from 'rxjs';

/**
 * This function checks if a string can be parsed as JSON.
 * @param jsonString
 * @returns Can the string be parsed as JSON?
 */
function canBeParsed(jsonString: string): boolean {
  try {
    const json = JSON.parse(jsonString);
    return typeof json === 'object';
  } catch (error) {
    return false;
  }
}

/**
 * This helper function extracts all keys from a JSON object.
 * @param obj
 * @param keys
 * @returns All keys from the JSON object.
 */
function extractKeys(obj: any, keys: string[] = []): string[] {
  if (typeof obj === 'object' && obj !== null) {
    Object.keys(obj).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        keys.push(key);
        extractKeys(obj[key], keys);
      }
    });
  }
  return keys;
}

/**
 * This function checks if a JSON string has leading or trailing spaces in the keys.
 * @param jsonString
 * @returns Does the JSON string have leading or trailing spaces in the keys?
 */
function hasLeadingOrTrailingSpaces(jsonString: string): boolean {
  try {
    const json = JSON.parse(jsonString);
    const keys = extractKeys(json);
    return keys.some((key) => key.startsWith(' ') || key.endsWith(' '));
  } catch (error) {
    return false;
  }
}

/**
 * This function checks if a JSON string has duplicate keys on the same hierachical layer.
 * @param jsonString
 * @returns Does the JSON string have duplicate keys?
 */
function hasDuplicateKeys(jsonString: string): boolean {
  const re = /"([^"]+)":/g;
  const matches = jsonString.match(re);
  const regexKeys: string[] = [];

  if (matches) {
    matches.forEach((match) => {
      const key = match.replace(/"/g, '').replace(/:/g, '');
      regexKeys.push(key);
    });
  }
  // JSON.parse() removes automatically duplicate keys on the same layer.
  const parsedKeys: string[] = extractKeys(JSON.parse(jsonString));
  // If the number of keys in the parsed JSON object is different from the number of keys in the regex, there are duplicate keys already deleted by JSON.parse().
  if (parsedKeys.length !== regexKeys.length) {
    return true;
  }
  return false;
}

/**
 * This function is a custom validator for JSON strings.
 */
export function isValidJSON(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    if (canBeParsed(control.value) && !hasDuplicateKeys(control.value) && !hasLeadingOrTrailingSpaces(control.value)) {
      return null;
    }
    return { isNotJson: control.value };
  };
}

/**
 * This function is a custom validator for Files with JSON strings.
 */
export function jsonFilesValidator(): AsyncValidatorFn {
  // eslint-disable-next-line prettier/prettier
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    const files = control.value as File[];
    if (!files || files.length === 0 || [...files].some((file) => file.type !== 'application/json')) {
      return from(Promise.resolve(null));
    }
    const fileValidations = [...files].map((file) => {
      return new Promise<boolean>((resolve) => {
        const reader = new FileReader();
        reader.onload = () => {
          // hasLeadingOrTrailingSpaces might have a usecase but not for importing JSON files
          if (canBeParsed(reader.result as string)) {
            resolve(true);
          } else {
            resolve(false);
          }
        };
        reader.onerror = () => resolve(false);
        // emits .onload event
        reader.readAsText(file);
      });
    });
    return from(Promise.all(fileValidations)).pipe(
      map((results) => {
        const allValid = results.every((isValid) => isValid);
        return allValid ? null : { invalidJsonFiles: true };
      }),
      catchError(() => from(Promise.resolve({ invalidJsonFiles: true })))
    );
  };
}
