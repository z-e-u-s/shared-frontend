import { AbstractControl, FormControl } from '@angular/forms';
import { invalidLogprepCharsValidator } from './invalid-logprep-chars.validator';

describe('invalidLogprepCharsValidator()', () => {
  let control: AbstractControl;

  beforeEach(() => {
    control = new FormControl();
  });

  it('should return null if value does not contain invalid characters', () => {
    control.setValue('valid_name');
    const validator = invalidLogprepCharsValidator();
    const result = validator(control);
    expect(result).toBeNull();
  });

  it('should return an error object if value contains invalid characters', () => {
    control.setValue('invalid:name');
    const validator = invalidLogprepCharsValidator();
    const result = validator(control);
    expect(result).toEqual({ invalidLogprepChars: true });
  });

  it('should return an error object if value contains spaces', () => {
    control.setValue('invalid name');
    const validator = invalidLogprepCharsValidator();
    const result = validator(control);
    expect(result).toEqual({ invalidLogprepChars: true });
  });

  it('should return an error object if value contains backslashes', () => {
    control.setValue('invalid\\name');
    const validator = invalidLogprepCharsValidator();
    const result = validator(control);
    expect(result).toEqual({ invalidLogprepChars: true });
  });

  it('should return an error object if value contains curly braces', () => {
    control.setValue('invalid{name}');
    const validator = invalidLogprepCharsValidator();
    const result = validator(control);
    expect(result).toEqual({ invalidLogprepChars: true });
  });
});
