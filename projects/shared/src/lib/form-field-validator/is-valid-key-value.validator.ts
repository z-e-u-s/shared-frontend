import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

/**
 * Check if all JSON object values have valid data types.
 * @param json A JSON object.
 * @returns True if the key value pairs are valid, false otherwise.
 */
function hasValidKeyValueFormat(jsonObject: any): boolean {
  // The initial object must exist as such.
  if (!jsonObject || typeof jsonObject !== 'object') {
    return false;
  }

  const validates: boolean[] = [];

  // Recursive function to check the key value pairs.
  function wrapper(value: any) {
    // The value can be a string.
    if (typeof value === 'string') {
      validates.push(true);
      return;
    }

    // The value can be an array of strings.
    if (Array.isArray(value)) {
      if (value.every((item: any) => typeof item === 'string')) {
        validates.push(true);
      } else {
        validates.push(false);
      }
      return;
    }

    // The value can be an object itself which values must fulfill the conditions above (recursively).
    if (typeof value === 'object') {
      Object.keys(value).forEach((key) => {
        wrapper(value[key]);
      });
      return;
    }

    // If none of the conditions above are met, the key value is not valid.
    validates.push(false);
  }
  wrapper(jsonObject);

  if (validates.includes(false)) {
    return false;
  }
  return true;
}

export function isKeyValue(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    try {
      const jsonObject = JSON.parse(control.value);
      if (hasValidKeyValueFormat(jsonObject) === false) {
        return { isNotKeyValue: control.value };
      }
      return null;
    } catch (error) {
      return { isNotKeyValue: control.value };
    }
  };
}

export function isRequiredKeyValue(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return { isNotKeyValue: control.value };
    }
    try {
      const jsonObject = JSON.parse(control.value);
      if (Object.keys(jsonObject).length === 0) {
        return { isNotKeyValue: control.value };
      }
      if (hasValidKeyValueFormat(jsonObject) === false) {
        return { isNotKeyValue: control.value };
      }
      return null;
    } catch (error) {
      return { isNotKeyValue: control.value };
    }
  };
}
