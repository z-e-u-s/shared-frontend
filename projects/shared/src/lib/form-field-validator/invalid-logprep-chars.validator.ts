import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { invalidLogprepCharsPattern } from '../const/regex-list';

/** This sync function validates a field by given regex pattern. If the input field contains invalid characters, it will set the searchFormControl errors to invalidLogprepChars and displays an error message. Which characters are invalid is determined Logprep and its dependencies which are not able to process these. */
export function invalidLogprepCharsValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const matchesPattern = invalidLogprepCharsPattern.test(control.value);
    return matchesPattern ? { invalidLogprepChars: true } : null;
  };
}
