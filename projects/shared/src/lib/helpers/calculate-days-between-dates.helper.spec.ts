import { calculateDaysBetweenDates } from './calculate-days-between-dates.helper';

describe('calculateDaysBetweenDates', () => {
  it('should calculate the amount of dates between two dates', () => {
    const date1 = new Date('2023-02-10');
    const date2 = new Date('2023-02-05');
    expect(calculateDaysBetweenDates(date1, date2)).toEqual(5);
    expect(calculateDaysBetweenDates(date2, date1)).toEqual(5);
  });

  it('should be able to return 0', () => {
    const date1 = new Date('2023-02-10');
    expect(calculateDaysBetweenDates(date1, date1)).toEqual(0);
  });
});
