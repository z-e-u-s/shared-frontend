/** This method checks if two arrays are equal.
 * @param arr1 The first array
 * @param arr2 The second array
 */
export function areArraysEqual(arr1: string[], arr2: string[]): boolean {
  if (arr1.length !== arr2.length) return false;
  const sortArr1 = [...arr1].sort();
  const sortArr2 = [...arr2].sort();
  return sortArr1.every((value, index) => value === sortArr2[index]);
}
