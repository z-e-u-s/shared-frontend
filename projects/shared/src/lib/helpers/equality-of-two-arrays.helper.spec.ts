import { areArraysEqual } from './equality-of-two-arrays.helper';

describe('arraysEqual()', () => {
  it('should match two same flat arrays as equal', () => {
    const isEqual = areArraysEqual(['1', '2', '3'], ['1', '2', '3']);
    expect(isEqual).toBeTruthy();
  });

  it('should match two similar flat arrays as equal', () => {
    const isEqual = areArraysEqual(['3', '2', '1'], ['1', '2', '3']);
    expect(isEqual).toBeTruthy();
  });

  it('should match two different flat arrays as not equal', () => {
    const isEqual = areArraysEqual(['4', '5', '6'], ['1', '2', '3']);
    expect(isEqual).toBeFalsy();
  });
});
