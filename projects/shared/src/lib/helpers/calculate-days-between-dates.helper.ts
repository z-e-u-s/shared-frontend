/** This function takes two dates as arguments and returns the amount of whole days between them.
 * The order is determined by the function
 */
export function calculateDaysBetweenDates(date1: Date, date2: Date): number {
  const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

  if (date2 > date1) {
    return Math.round(Math.abs((date2.getTime() - date1.getTime()) / oneDay));
  }
  return Math.round(Math.abs((date1.getTime() - date2.getTime()) / oneDay));
}
