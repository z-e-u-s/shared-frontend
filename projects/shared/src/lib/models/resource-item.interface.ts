/**
 * A ResourceItem could be a menu item or a breadcrumb item, or any other item that needs to be
 * enabled/disabled based on user roles.
 * It could be a tree structure (like in the menu items case), each item can have children, and each item can have userRoles.
 */
export interface ResourceItem {
  userRoles?: any[]; // UserRoles[] --> if userRoles is not defined, the item is visible to all users
  children?: ResourceItem[];
}
