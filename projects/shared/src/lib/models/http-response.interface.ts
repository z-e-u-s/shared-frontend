import { HttpResponseDetails } from './http-response-details.interface';

export interface HttpResponse<T> {
  data: T[];
  message?: string;
  errors?: HttpResponseDetails[];
}
