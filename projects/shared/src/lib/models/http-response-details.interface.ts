import { AlertType } from '../components/alert/alert.types';

export interface HttpResponseDetails {
  code: AlertType; // 'warning' | 'info' | ..
  description: string;
  fieldDescription: string;
  helpText: string;
  path: string;
  type: ErrorType;
}

export type ErrorType = 'ValidationError' | 'Warning' | 'LogprepProcessLog' | any;
