export type RequestResponseMessageType =
  | 'createdSuccessfully'
  | 'updatedSuccessfully'
  | 'deletedSuccessfully'
  | 'saveError'
  | 'activationInProgress'
  | 'activationSuccessful'
  | 'deactivationSuccessful';
