import { Sort } from '@angular/material/sort';
import { HttpResponse } from './http-response.interface';

/**
 * The data needed for the pagination.
 * @param resultCount The total number of results.
 * @param currentPageIndex The current page index.
 * @param totalPages The total number of pages.
 * @param currentPageSize The current page size.
 * @param displaySoftDeletedElements Whether to display soft deleted elements.
 */
export interface PaginationData {
  resultCount: number;
  currentPageIndex: number;
  totalPages: number;
  currentPageSize: number;
}

/**
 * The pagination data received by the API.
 * @param data
 * @param message
 * @param errors
 * @param pagination The pagination data.
 * @param pagination.count The total number of results.
 * @param pagination.next The next page URL.
 * @param pagination.previous The previous page URL.
 */
export interface HttpPaginationResponse<T> extends HttpResponse<T> {
  pagination?: {
    count: number;
    next: string | null;
    previous: string | null;
  };
}

/**
 * The data needed when emitting the GoToPage event
 */
export interface GoToPageEvent {
  newPageIndex: number;
  newPageSize: number;
  searchTerm?: string | undefined;
  sortEvent?: Sort | undefined;
  displaySoftDeletedElements: boolean;
}
