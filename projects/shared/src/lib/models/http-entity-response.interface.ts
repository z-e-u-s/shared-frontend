import { HttpResponseDetails } from './http-response-details.interface';

export interface HttpEntityResponse<T> {
  data: T;
  message?: string;
  errors?: HttpResponseDetails[];
}
