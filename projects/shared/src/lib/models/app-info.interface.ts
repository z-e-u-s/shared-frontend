export interface AppInfo {
  appShortName: string;
  appName: string;
  isProduction: boolean;
}
