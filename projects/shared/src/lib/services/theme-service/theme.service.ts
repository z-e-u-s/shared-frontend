import { Inject, Injectable, signal } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Scheme, Theme } from '../../modules/layout/models/scheme.types';

export const LOCALSTORAGE_THEME = 'theme';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  public default: Theme = 'light';
  public themeChanged = signal(this.theme);

  constructor(@Inject(DOCUMENT) private document: any) {}

  public get theme(): Theme {
    return (localStorage.getItem(LOCALSTORAGE_THEME) as Theme) ?? this.default;
  }

  public set theme(value: Theme) {
    localStorage.setItem(LOCALSTORAGE_THEME, value);
    this.themeChanged.set(value);
  }

  public get isDark(): boolean {
    return this.theme === 'dark';
  }

  /**
   * This methods triggers refresh of the UI based on the selected scheme.
   * It removes previous 'light' or 'dark' class names from the <body> and adds the appropriate class name for the selected scheme.
   * @param scheme
   */
  refreshUI(scheme: Scheme): void {
    // Remove class names from body for all schemes
    this.document.body.classList.remove('light', 'dark');

    // Add class name to body for the currently selected scheme
    this.document.body.classList.add(scheme);
  }
}
