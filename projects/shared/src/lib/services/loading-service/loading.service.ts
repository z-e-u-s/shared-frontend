import { Injectable, signal } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LoadingService {
  private isLoadingSignal = signal<boolean>(false);

  isLoading = this.isLoadingSignal.asReadonly();

  public setLoadingState(newLoadingState: boolean): void {
    this.isLoadingSignal.set(newLoadingState);
  }
}
