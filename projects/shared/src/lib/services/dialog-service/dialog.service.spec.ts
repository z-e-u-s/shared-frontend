import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { of } from 'rxjs';
import { DialogService } from './dialog.service';

describe('DialogService', () => {
  let service: DialogService;
  let matDialog: MatDialog;

  beforeEach(() => {
    matDialog = { open: jest.fn() } as unknown as MatDialog;
    service = new DialogService(matDialog);
  });

  it('should create DialogService', () => {
    expect(service).toBeTruthy();
  });

  it('should open a delete confirmation dialog and execute callback on confirmation', () => {
    const onConfirmCallback = jest.fn();
    const arg = 'someArgument';
    const itemIdentifier = 'item123';

    jest.spyOn(matDialog, 'open').mockReturnValue({
      afterClosed: () => of(true), // Simulate dialog confirmation
    } as MatDialogRef<any, any>);

    service.onClickDelete(onConfirmCallback, arg, itemIdentifier);

    expect(matDialog.open).toHaveBeenCalledWith(expect.anything(), {
      disableClose: true,
      data: expect.objectContaining({
        hasActions: true,
        mode: 'delete',
        title: 'shared.MESSAGES.CONFIRM_DELETE.TITLE',
        itemIdentifier,
      }),
    });

    // Ensure the callback is executed on confirmation
    expect(onConfirmCallback).toHaveBeenCalledWith(arg);
  });

  it('should open a cancel confirmation dialog and execute callback on confirmation', () => {
    const onConfirmCallback = jest.fn();
    const arg = 'someArgument';

    jest.spyOn(matDialog, 'open').mockReturnValue({
      afterClosed: () => of(true), // Simulate dialog confirmation
    } as MatDialogRef<any, any>);

    service.onClickCancel(onConfirmCallback, arg);

    expect(matDialog.open).toHaveBeenCalledWith(expect.anything(), {
      disableClose: true,
      data: expect.objectContaining({
        hasActions: true,
        mode: 'confirmAction',
        title: 'shared.MESSAGES.CONFIRM_CANCEL.TITLE',
      }),
    });

    // Ensure the callback is executed on confirmation
    expect(onConfirmCallback).toHaveBeenCalledWith(arg);
  });

  it('should open a restore confirmation dialog and execute callback on confirmation', () => {
    const onConfirmCallback = jest.fn();
    const arg = 'someArgument';

    jest.spyOn(matDialog, 'open').mockReturnValue({
      afterClosed: () => of(true), // Simulate dialog confirmation
    } as MatDialogRef<any, any>);

    service.onClickRestore(onConfirmCallback, arg);

    expect(matDialog.open).toHaveBeenCalledWith(expect.anything(), {
      disableClose: true,
      data: expect.objectContaining({
        hasActions: true,
        mode: 'confirmAction',
        title: 'shared.MESSAGES.CONFIRM_CANCEL.TITLE',
      }),
    });

    // Ensure the callback is executed on confirmation
    expect(onConfirmCallback).toHaveBeenCalledWith(arg);
  });
});
