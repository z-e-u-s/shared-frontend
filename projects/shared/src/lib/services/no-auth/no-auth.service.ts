import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NoAuthService {
  getNoAuthUser() {
    return {
      username: 'system user',
      email: 'system@example.de',
      firstName: 'system',
      lastName: 'user',
    };
  }
}
