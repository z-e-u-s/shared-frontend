import { TestBed } from '@angular/core/testing';

import { NoAuthService } from './no-auth.service';

describe('NoAuthService', () => {
  let service: NoAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoAuthService],
    });
    service = TestBed.inject(NoAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return dummy no auth user', () => {
    const res = service.getNoAuthUser();
    expect(res).toStrictEqual({
      username: 'system user',
      email: 'system@example.de',
      firstName: 'system',
      lastName: 'user',
    });
  });
});
