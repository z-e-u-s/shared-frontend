import { Injectable, inject } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoadingService } from '../services/loading-service/loading.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  private readonly loadingService = inject(LoadingService);

  private totalRequests = 0;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.totalRequests += 1;
    this.loadingService.setLoadingState(true);

    return next.handle(request).pipe(
      finalize(() => {
        // eslint-disable-next-line no-plusplus
        this.totalRequests--;
        if (this.totalRequests === 0) {
          this.loadingService.setLoadingState(false);
        }
      })
    );
  }
}
