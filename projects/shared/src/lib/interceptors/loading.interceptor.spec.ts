import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockProvider } from 'ng-mocks';
import { LoadingInterceptor } from './loading.interceptor';
import { LoadingService } from '../services/loading-service/loading.service';

describe('LoadingInterceptor', () => {
  let interceptor: LoadingInterceptor;
  let loadingService: LoadingService;
  let firstHttpObservableMock: BehaviorSubject<HttpEvent<any>>;
  let secondHttpObservableMock: BehaviorSubject<HttpEvent<any>>;
  let thirdHttpObservableMock: BehaviorSubject<HttpEvent<any>>;
  let firstHttpHandler: HttpHandler;
  let secondHttpHandler: HttpHandler;
  let thirdHttpHandler: HttpHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoadingInterceptor, MockProvider(LoadingService)],
    });

    loadingService = TestBed.inject(LoadingService);
    interceptor = TestBed.inject(LoadingInterceptor);
    // everything related to HttpHandler mock needs to be set up again before each test
    // as each test may run complete() on the corresponding Subject
    // after which this Subject can not be used again
    firstHttpObservableMock = new BehaviorSubject<HttpEvent<any>>({} as HttpEvent<any>);
    secondHttpObservableMock = new BehaviorSubject<HttpEvent<any>>({} as HttpEvent<any>);
    thirdHttpObservableMock = new BehaviorSubject<HttpEvent<any>>({} as HttpEvent<any>);
    firstHttpHandler = { handle: jest.fn().mockReturnValue(firstHttpObservableMock) } as HttpHandler;
    secondHttpHandler = { handle: jest.fn().mockReturnValue(secondHttpObservableMock) } as HttpHandler;
    thirdHttpHandler = { handle: jest.fn().mockReturnValue(thirdHttpObservableMock) } as HttpHandler;
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });

  it('should call setLoadingState to true on each request', (done) => {
    jest.spyOn(loadingService, 'setLoadingState');

    expect(interceptor['totalRequests']).toBe(0);

    interceptor.intercept(new HttpRequest<any>('GET', '/data'), firstHttpHandler).subscribe(() => done());
    expect(interceptor['totalRequests']).toBe(1);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(true);
  });

  it('should increment totalRequests on each request', () => {
    jest.spyOn(loadingService, 'setLoadingState');

    expect(interceptor['totalRequests']).toBe(0);

    interceptor.intercept(new HttpRequest<any>('GET', '/data'), firstHttpHandler);
    expect(interceptor['totalRequests']).toBe(1);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(true);

    interceptor.intercept(new HttpRequest<any>('POST', '/data', {}), secondHttpHandler);
    expect(interceptor['totalRequests']).toBe(2);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(true);

    interceptor.intercept(new HttpRequest<any>('PUT', '/data', {}), thirdHttpHandler);
    expect(interceptor['totalRequests']).toBe(3);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(true);

    expect(loadingService.setLoadingState).toHaveBeenCalledTimes(3);
  });

  it('should decrement totalRequests and set loading to false when totalRequests is 0', (done) => {
    jest.spyOn(loadingService, 'setLoadingState');

    expect(interceptor['totalRequests']).toBe(0);

    interceptor.intercept(new HttpRequest<any>('GET', '/test-url'), firstHttpHandler).subscribe();
    expect(interceptor['totalRequests']).toBe(1);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(true);

    interceptor.intercept(new HttpRequest<any>('GET', '/test-url'), secondHttpHandler).subscribe(() => done());
    expect(interceptor['totalRequests']).toBe(2);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(true);

    firstHttpObservableMock.complete();
    expect(interceptor['totalRequests']).toBe(1);
    // should not call setLoadingState to false here

    secondHttpObservableMock.complete();
    expect(interceptor['totalRequests']).toBe(0);
    expect(loadingService.setLoadingState).toHaveBeenCalledWith(false);
    expect(loadingService.setLoadingState).toHaveBeenCalledTimes(3);
  });

  it('should set isLoading to false on error', (done) => {
    jest.spyOn(loadingService, 'setLoadingState');

    expect(interceptor['totalRequests']).toBe(0);

    firstHttpHandler.handle = jest.fn().mockReturnValue(of(new Error()));

    interceptor.intercept(new HttpRequest<any>('GET', '/data'), firstHttpHandler).subscribe(() => {
      done();
    });
    expect(interceptor['totalRequests']).toBe(0);
    expect(loadingService.setLoadingState).toHaveBeenCalledTimes(2);
    expect(loadingService.setLoadingState).toHaveBeenNthCalledWith(1, true);
    expect(loadingService.setLoadingState).toHaveBeenNthCalledWith(2, false);
  });
});
