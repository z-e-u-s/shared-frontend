import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatTooltipModule } from '@angular/material/tooltip';
import { ToggleButtonComponent } from './toggle-button.component';

describe('ToggleButtonComponent', () => {
  let component: ToggleButtonComponent;
  let fixture: ComponentFixture<ToggleButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ToggleButtonComponent, MatTooltipModule],
    });
    fixture = TestBed.createComponent(ToggleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onClickToggle should not toggle button if buttonStateIsLocked is true', () => {
    component.buttonStateIsLocked = true;
    component.onClickToggle();

    expect(component.isChecked).toBe(false);
  });

  it('onClickToggle should set isChecked to true if isChecked was false and emit the changed state', () => {
    jest.spyOn(component.buttonStateChanged, 'emit');
    component.isChecked = false;
    component.onClickToggle();

    expect(component.isChecked).toBe(true);
    expect(component.buttonStateChanged.emit).toHaveBeenCalledWith(true);
  });

  it('onClickToggle should set isChecked to false if isChecked was true and emit the changed state', () => {
    jest.spyOn(component.buttonStateChanged, 'emit');
    component.isChecked = true;
    component.onClickToggle();

    expect(component.isChecked).toBe(false);
    expect(component.buttonStateChanged.emit).toHaveBeenCalledWith(false);
  });
});
