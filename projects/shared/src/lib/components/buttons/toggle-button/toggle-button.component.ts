import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'lib-toggle-button',
  templateUrl: './toggle-button.component.html',
  standalone: true,
  imports: [CommonModule, MatTooltipModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToggleButtonComponent {
  @Input({ required: true }) isChecked: boolean = false;
  @Input() buttonStateIsLocked: boolean = false;
  @Input() colorScheme: 'primary' | 'accent' = 'primary';
  /** Input for an optional tooltip text, has to be translated beforehand */
  @Input() toolTip: string = '';
  @Output() buttonStateChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  onClickToggle(): void {
    if (this.buttonStateIsLocked) {
      return;
    }
    this.isChecked = !this.isChecked;
    this.buttonStateChanged.emit(this.isChecked);
  }
}
