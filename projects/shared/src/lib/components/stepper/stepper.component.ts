import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  inject,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslocoModule } from '@jsverse/transloco';
import { SvgIconComponent } from 'angular-svg-icon';
import { StepperStep } from './models/stepper-step.interface';
import { IconButtonDirective } from './directives/icon-button.directive';

/** How to use:
 *
 * - This component requires an array of StepperStep objects and a stepperContext string as input
 * - the stepperContext is used at the last step to show what item is being created/edited
 * - use the inputs isLoading and goForwardIsDisabled optionally to customize the behavior
 * - the component will keep track of current and completed steps, so you don't need to update the original array in the parent component
 * - nest any templates inside the lib-stepper element to display them
 * - if you want to render specific content based on the current step, keep track of currentStepIndex in your parent component (e.g.: originalStepsArray[currentStepIndex])
 * @ViewChild directive can be used to call the handler methods from outside the component (e.g. to skip a step)
 */
@Component({
  selector: 'lib-stepper',
  standalone: true,
  templateUrl: './stepper.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, TranslocoModule, MatTooltipModule, MatButtonModule, SvgIconComponent, IconButtonDirective],
})
export class StepperComponent implements OnInit {
  private readonly cdr = inject(ChangeDetectorRef);

  @Input({ required: true }) stepperSteps!: StepperStep[];
  @Input({ required: true }) stepperContext!: string;
  @Input() isLoading: boolean = false;
  @Input() goForwardIsDisabled: boolean = false;
  @Output() goBack: EventEmitter<number> = new EventEmitter<number>();
  @Output() goForward: EventEmitter<number> = new EventEmitter<number>();

  currentStepIndex: number = 0;

  ngOnInit(): void {
    this.stepperSteps[this.currentStepIndex].isCurrent = true;
  }

  /** This method updates the currentStepIndex and the stepperSteps and emits goBack if the action is successful
   * @param factor - the amount by which the currentStepIndex should be incremented
   * @ViewChild directive can be used to call this method from outside the component
   */
  handleGoBack(factor: number): void {
    // repeat the action 'factor' times
    for (let i = 0; i < factor; i += 1) {
      if (this.currentStepIndex > 0) {
        this.stepperSteps[this.currentStepIndex].isCompleted = false;
        this.stepperSteps[this.currentStepIndex].isCurrent = false;
        this.stepperSteps[this.currentStepIndex - 1].isCurrent = true;
        this.stepperSteps[this.currentStepIndex - 1].isCompleted = false;
        this.currentStepIndex -= 1;
        this.cdr.markForCheck();
        this.goBack.emit(this.currentStepIndex);
      }
    }
  }

  /** This method updates the currentStepIndex and the stepperSteps and emits goForward if the action is successful
   * @param factor - the amount by which the currentStepIndex should be incremented
   * @ViewChild directive can be used to call this method from outside the component
   */
  handleGoForward(factor: number): void {
    if (this.goForwardIsDisabled) {
      return;
    }
    // repeat the action 'factor' times
    for (let t = 0; t < factor; t += 1) {
      if (this.currentStepIndex < this.stepperSteps.length - 1) {
        this.stepperSteps[this.currentStepIndex].isCompleted = true;
        this.stepperSteps[this.currentStepIndex].isCurrent = false;
        this.stepperSteps[this.currentStepIndex + 1].isCurrent = true;
        this.currentStepIndex += 1;
        this.cdr.markForCheck();
        this.goForward.emit(this.currentStepIndex);
      }
    }
  }
}
