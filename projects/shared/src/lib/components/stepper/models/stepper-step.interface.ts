export interface StepperStep {
  name: string;
  title: string;
  isCompleted: boolean;
  isCurrent: boolean;
}
