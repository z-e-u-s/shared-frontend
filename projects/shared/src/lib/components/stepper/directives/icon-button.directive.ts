import { Directive, HostBinding } from '@angular/core';

/** Use this directive to create a rounded button that contains only an icon.
 *  It applies the following tailwind classes:
 * - rounded-full
 * - p-2
 * - shadow-sm
 * - focus-visible:outline
 * - focus-visible:outline-2
 * - focus-visible:outline-offset-2
 * - disabled:opacity-50
 * - disabled:cursor-not-allowed
 */
@Directive({
  selector: '[libIconButton]',
  standalone: true,
})
export class IconButtonDirective {
  @HostBinding('class')
  classes =
    `rounded-full p-2 shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2  disabled:opacity-50 disabled:cursor-not-allowed`;
}
