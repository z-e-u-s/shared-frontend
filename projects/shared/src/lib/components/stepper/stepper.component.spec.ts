import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SvgIconComponent } from 'angular-svg-icon';
import { MockComponent, MockModule } from 'ng-mocks';
import { StepperComponent } from './stepper.component';
import { MOCK_STEPPER_STEPS } from '../../../test-data/stepper-steps';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';

describe('StepperComponent', () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        StepperComponent,
        NoopAnimationsModule,
        getTranslocoTestingModule(),
        MockModule(MatButtonModule),
        MockModule(MatTooltipModule),
        MockComponent(SvgIconComponent),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;
    component.stepperSteps = MOCK_STEPPER_STEPS;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should set first step as current', () => {
    component.ngOnInit();

    expect(component.stepperSteps[0].isCurrent).toEqual(true);
  });

  it('handleGoBack should decrement currentStepIndex, update stepperSteps and emit goBack with updated index', () => {
    jest.spyOn(component.goBack, 'emit');
    component.currentStepIndex = 2;

    component.handleGoBack(1);

    expect(component.currentStepIndex).toEqual(1);
    expect(component.stepperSteps[2].isCurrent).toEqual(false);
    expect(component.stepperSteps[2].isCompleted).toEqual(false);
    expect(component.stepperSteps[1].isCurrent).toEqual(true);
    expect(component.stepperSteps[1].isCompleted).toEqual(false);
    expect(component.currentStepIndex).toEqual(1);
    expect(component.goBack.emit).toHaveBeenCalledTimes(1);
    expect(component.goBack.emit).toHaveBeenCalledWith(1);
  });

  it('handleGoBack should not decrement currentStepIndex, not update stepperSteps and not emit goBack if currentStepIndex is 0', () => {
    jest.spyOn(component.goBack, 'emit');
    component.currentStepIndex = 0;

    component.handleGoBack(1);

    expect(component.currentStepIndex).toEqual(0);
    expect(component.stepperSteps[0].isCurrent).toEqual(true);
    expect(component.stepperSteps[0].isCompleted).toEqual(false);
    expect(component.currentStepIndex).toEqual(0);
    expect(component.goBack.emit).not.toHaveBeenCalled();
  });

  it('handleGoForward should increment currentStepIndex, update stepperSteps and emit goForward with updated index', () => {
    jest.spyOn(component.goForward, 'emit');
    component.currentStepIndex = 1;

    component.handleGoForward(1);

    expect(component.currentStepIndex).toEqual(2);
    expect(component.stepperSteps[1].isCurrent).toEqual(false);
    expect(component.stepperSteps[1].isCompleted).toEqual(true);
    expect(component.stepperSteps[2].isCurrent).toEqual(true);
    expect(component.currentStepIndex).toEqual(2);
    expect(component.goForward.emit).toHaveBeenCalledTimes(1);
    expect(component.goForward.emit).toHaveBeenCalledWith(2);
  });

  it('handleGoForward should increment currentStepIndex by 2, update stepperSteps and emit updated index when called with factor 2', () => {
    jest.spyOn(component.goForward, 'emit');
    component.currentStepIndex = 0;

    component.handleGoForward(2);

    expect(component.stepperSteps[0].isCurrent).toEqual(false);
    expect(component.stepperSteps[0].isCompleted).toEqual(true);
    expect(component.stepperSteps[1].isCurrent).toEqual(false);
    expect(component.stepperSteps[1].isCompleted).toEqual(true);
    expect(component.stepperSteps[2].isCurrent).toEqual(true);
    expect(component.stepperSteps[2].isCompleted).toEqual(false);
    expect(component.currentStepIndex).toEqual(2);
    expect(component.goForward.emit).toHaveBeenCalledTimes(2);
  });

  it('handleGoForward should not increment currentStepIndex, not update stepperSteps and not emit goForward if current step is last step', () => {
    jest.spyOn(component.goForward, 'emit');
    component.currentStepIndex = 2;

    component.handleGoForward(1);

    expect(component.stepperSteps[2].isCurrent).toEqual(true);
    expect(component.stepperSteps[2].isCompleted).toEqual(false);
    expect(component.currentStepIndex).toEqual(2);
    expect(component.goForward.emit).not.toHaveBeenCalled();
  });
});
