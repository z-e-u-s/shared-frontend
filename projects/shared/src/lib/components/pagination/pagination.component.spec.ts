import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SimpleChange, SimpleChanges } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';
import { PaginationComponent } from './pagination.component';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [],
      imports: [NoopAnimationsModule, getTranslocoTestingModule(), PaginationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;

    // mock pagination data
    component.paginationData = {
      currentPageIndex: 1,
      totalPages: 0,
      currentPageSize: 10,
      resultCount: 200,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call calculateTotalPages() when parent has changed pagination data', () => {
    const newPaginationData = {
      displaySoftDeletedElements: false,
      currentPageIndex: 1,
      totalPages: 40,
      currentPageSize: 10,
      resultCount: 400,
    };
    const changes: SimpleChanges = {
      paginationData: new SimpleChange(null, newPaginationData, false),
    };
    jest.spyOn(component, 'calculateTotalPages');
    component.paginationData = newPaginationData;

    component.ngOnChanges(changes);

    expect(component.calculateTotalPages).toHaveBeenCalledTimes(1);
    expect(component.calculateTotalPages).toHaveBeenCalledWith(
      newPaginationData.resultCount,
      newPaginationData.currentPageSize
    );
    expect(component.paginationData.currentPageIndex).toBe(1);
    expect(component.paginationData.totalPages).toBe(40);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(400);
  });

  it('should call paginationService.updatePagination() and goToPage when calling updateTotalPagesAndGoToPage()', () => {
    // Pagination data haven't been changed yet. So, the data should be the same as the mock data.
    jest.spyOn(component.goToPage, 'emit');
    jest.spyOn(component, 'calculateTotalPages');
    component.updateTotalPagesAndGoToPage();

    expect(component.goToPage.emit).toHaveBeenCalledTimes(1);
    expect(component.goToPage.emit).toHaveBeenCalledWith({
      displaySoftDeletedElements: false,
      newPageIndex: 1,
      newPageSize: 10,
    });
    expect(component.calculateTotalPages).toHaveBeenCalledTimes(1);
    expect(component.calculateTotalPages).toHaveBeenCalledWith(
      component.paginationData.resultCount,
      component.paginationData.currentPageSize
    );
    expect(component.paginationData.currentPageIndex).toBe(1);
    expect(component.paginationData.totalPages).toBe(20);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should call updateTotalPagesAndGoToPage() when calling onLoadFirstPage()', () => {
    jest.spyOn(component, 'updateTotalPagesAndGoToPage');
    component.onLoadFirstPage();

    expect(component.updateTotalPagesAndGoToPage).toHaveBeenCalledTimes(1);
    expect(component.paginationData.currentPageIndex).toBe(1);
    expect(component.paginationData.totalPages).toBe(20);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should call updateTotalPagesAndGoToPage() when calling onLoadLastPage()', () => {
    jest.spyOn(component, 'updateTotalPagesAndGoToPage');
    component.paginationData = {
      currentPageIndex: 1,
      totalPages: 20,
      currentPageSize: 10,
      resultCount: 200,
    };
    component.onLoadLastPage();

    expect(component.updateTotalPagesAndGoToPage).toHaveBeenCalledTimes(1);
    expect(component.paginationData.currentPageIndex).toBe(20);
    expect(component.paginationData.totalPages).toBe(20);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should call updateTotalPagesAndGoToPage() when calling onLoadPreviousPage()', () => {
    jest.spyOn(component, 'updateTotalPagesAndGoToPage');
    component.onLoadPreviousPage(2);

    expect(component.updateTotalPagesAndGoToPage).toHaveBeenCalledTimes(1);
    expect(component.paginationData.currentPageIndex).toBe(1);
    expect(component.paginationData.totalPages).toBe(20);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should call updateTotalPagesAndGoToPage() when calling onLoadNextPage()', () => {
    jest.spyOn(component, 'updateTotalPagesAndGoToPage');
    component.onLoadNextPage(component.paginationData.currentPageIndex);

    expect(component.updateTotalPagesAndGoToPage).toHaveBeenCalledTimes(1);
    expect(component.paginationData.currentPageIndex).toBe(2);
    expect(component.paginationData.totalPages).toBe(20);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should call updateTotalPagesAndGoToPage() when calling onChangePageSize()', () => {
    jest.spyOn(component, 'updateTotalPagesAndGoToPage');
    component.onChangePageSize(20);

    expect(component.updateTotalPagesAndGoToPage).toHaveBeenCalledTimes(1);
    expect(component.paginationData.currentPageIndex).toBe(1);
    expect(component.paginationData.totalPages).toBe(10);
    expect(component.paginationData.currentPageSize).toBe(20);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should call service.updatePagination() when calling updateTotalPagesAndGoToPage()', () => {
    jest.spyOn(component, 'calculateTotalPages');
    component.paginationData = {
      currentPageIndex: 1,
      totalPages: 0,
      currentPageSize: 10,
      resultCount: 200,
    };
    component.updateTotalPagesAndGoToPage();

    expect(component.calculateTotalPages).toHaveBeenCalledWith(
      component.paginationData.resultCount,
      component.paginationData.currentPageSize
    );
    expect(component.paginationData.currentPageIndex).toBe(1);
    expect(component.paginationData.totalPages).toBe(20);
    expect(component.paginationData.currentPageSize).toBe(10);
    expect(component.paginationData.resultCount).toBe(200);
  });

  it('should compute the total page amount when calling calculateTotalPages()', () => {
    const result = component.calculateTotalPages(200, 10);
    expect(result).toBe(20);
  });
});
