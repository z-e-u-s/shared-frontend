import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';

import { GoToPageEvent, PaginationData } from '../../models/pagination.interface';
import { TranslocoCoreModule } from '../../transloco/transloco.module';
import { PAGE_SIZE_OPTIONS } from './page-size-options.const';

@Component({
  selector: 'lib-pagination',
  templateUrl: './pagination.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatTooltipModule, TranslocoCoreModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatSelectModule],
})
export class PaginationComponent implements OnChanges {
  /** The data received from the API for feeding the paginator. */
  @Input({ required: true }) paginationData!: PaginationData;
  @Input() pageSizeOptions: number[] = PAGE_SIZE_OPTIONS;
  /** The event emitted when the user changes the page size or clicks on a page button. The event data should be used for the request. */
  @Output() goToPage = new EventEmitter<GoToPageEvent>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['paginationData']?.currentValue) {
      this.paginationData.totalPages = this.calculateTotalPages(
        this.paginationData.resultCount,
        this.paginationData.currentPageSize
      );
    }
  }

  /**
   * This method calculates the total pages.
   * @param resultCount The total number of table items.
   * @param currentPageSize The number of items that are loaded onto the page.
   */
  calculateTotalPages(resultCount: number, currentPageSize: number): number {
    return Math.ceil(resultCount / currentPageSize);
  }

  /**
   * This method updates the total number of pages with the current pagination state.
   * It also emits the goToPage event with the new page index and page size.
   */
  updateTotalPagesAndGoToPage(): void {
    this.paginationData.totalPages = this.calculateTotalPages(
      this.paginationData.resultCount,
      this.paginationData.currentPageSize
    );
    this.goToPage.emit({
      newPageIndex: this.paginationData!.currentPageIndex,
      newPageSize: this.paginationData!.currentPageSize,
      displaySoftDeletedElements: false,
    });
  }

  /** Load the previous TestExecution stack set.
   * @param newPageIndex
   */
  onLoadPreviousPage(newPageIndex: number): void {
    this.paginationData!.currentPageIndex = newPageIndex - 1;
    this.updateTotalPagesAndGoToPage();
  }

  /** Load the next TestExecution stack set.
   * @param newPageIndex
   */
  onLoadNextPage(newPageIndex: number): void {
    this.paginationData!.currentPageIndex = newPageIndex + 1;
    this.updateTotalPagesAndGoToPage();
  }

  /** Load the first TestExecution stack set. */
  onLoadFirstPage(): void {
    this.paginationData!.currentPageIndex = 1;
    this.updateTotalPagesAndGoToPage();
  }

  /** Load the last TestExecution stack set. */
  onLoadLastPage(): void {
    this.paginationData!.currentPageIndex = this.paginationData.totalPages;
    this.updateTotalPagesAndGoToPage();
  }

  /** Change the page size by user input. Default: 10 items per page.
   * @param newPageSize
   */
  onChangePageSize(newPageSize: number): void {
    this.paginationData!.currentPageIndex = 1;
    this.paginationData!.currentPageSize = newPageSize;
    this.updateTotalPagesAndGoToPage();
  }
}
