import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NgxJdenticonModule } from 'ngx-jdenticon';

@Component({
  selector: 'lib-user-icon-avatar',
  templateUrl: './user-icon-avatar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgxJdenticonModule],
})
export class UserIconAvatarComponent {
  @Input({ required: true }) name!: string;
}
