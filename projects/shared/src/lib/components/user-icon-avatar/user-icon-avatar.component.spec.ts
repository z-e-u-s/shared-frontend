import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserIconAvatarComponent } from './user-icon-avatar.component';

describe('UserIconAvatarComponent', () => {
  let component: UserIconAvatarComponent;
  let fixture: ComponentFixture<UserIconAvatarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [UserIconAvatarComponent],
    });
    fixture = TestBed.createComponent(UserIconAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
