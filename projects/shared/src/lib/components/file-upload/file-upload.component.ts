import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { CommonModule } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  Output,
  Self,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NgControl,
  ReactiveFormsModule,
  UntypedFormControl,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { filter, Subject, takeUntil } from 'rxjs';
import { jsonFilesValidator } from '../../form-field-validator/is-valid-json.validator';
import { yamlFilesValidator } from '../../form-field-validator/is-valid-yaml.validator';

let nextId = 0;

@Component({
  selector: 'lib-file-upload',
  templateUrl: './file-upload.component.html',
  standalone: true,
  imports: [CommonModule, MatFormFieldModule, ReactiveFormsModule],
  providers: [{ provide: MatFormFieldControl, useExisting: FileUploadComponent }],
})
// eslint-disable-next-line prettier/prettier
export class FileUploadComponent implements ControlValueAccessor, MatFormFieldControl<File[]>, OnDestroy {
  fileControl = new UntypedFormControl(undefined, [this.fileTypeValidator()]);
  fileName = '';
  errorState = false;
  stateChanges = new Subject<void>();
  focused = false;
  firstCointainerClick = true;
  controlType = 'lib-file-upload';
  static nextId = 0;
  // eslint-disable-next-line no-plusplus
  id = `lib-file-upload-${nextId++}`;
  describedBy = '';
  private destroy$ = new Subject<void>();

  @Output() selectedFiles = new EventEmitter<File[]>();
  @ViewChild('input', { read: ElementRef }) inputFileUpload: ElementRef | undefined;

  @HostBinding('attr.aria-describedby') ariaDescribedby: string | undefined;
  // @ts-ignore
  _value: File[] | null;
  @Input()
  get value(): File[] | null {
    return this.fileControl.value;
  }
  set value(files: File[] | null) {
    this.fileControl.setValue(files);
    this.stateChanges.next();
  }

  // eslint-disable-next-line no-underscore-dangle
  private _multiple: boolean = false;
  @Input()
  get multiple(): boolean {
    // eslint-disable-next-line no-underscore-dangle
    return this._multiple;
  }
  set multiple(value: boolean | string) {
    // eslint-disable-next-line no-underscore-dangle
    this._multiple = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  // eslint-disable-next-line no-underscore-dangle
  private _accept: string = 'text/yaml,.yaml,.yml';
  @Input({ required: true })
  get accept(): string {
    // eslint-disable-next-line no-underscore-dangle
    return this._accept;
  }
  set accept(value: string) {
    if (value.split(',').some((type) => type.trim() === 'application/json' || type.trim() === '.json')) {
      this.fileControl.addAsyncValidators(jsonFilesValidator());
    }
    if (
      value.split(',').some((type) => type.trim() === 'text/yaml' || type.trim() === '.yml' || type.trim() === '.yaml')
    ) {
      this.fileControl.addAsyncValidators(yamlFilesValidator());
    }
    // eslint-disable-next-line no-underscore-dangle
    this._accept = value;
    this.stateChanges.next();
  }

  // eslint-disable-next-line no-underscore-dangle
  private _placeholder: string = 'No file uploaded yet';
  @Input()
  get placeholder(): string {
    // eslint-disable-next-line no-underscore-dangle
    return this._placeholder;
  }
  set placeholder(value: string) {
    // eslint-disable-next-line no-underscore-dangle
    this._placeholder = value;
    this.stateChanges.next();
  }

  // eslint-disable-next-line no-underscore-dangle
  private _required = false;
  @Input()
  get required(): boolean {
    // eslint-disable-next-line no-underscore-dangle
    return this._required;
  }
  set required(value: boolean) {
    if (value) {
      this.fileControl.addValidators(Validators.required);
    }
    // eslint-disable-next-line no-underscore-dangle
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  // eslint-disable-next-line no-underscore-dangle
  private _disabled = false;

  @Input()
  get disabled(): boolean {
    // eslint-disable-next-line no-underscore-dangle
    return this._disabled;
  }
  set disabled(value: boolean) {
    if (value) {
      this.fileControl.disable();
    } else {
      this.fileControl.enable();
    }
    // eslint-disable-next-line no-underscore-dangle
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  get empty() {
    return !this.fileControl.value;
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  wasPreviouslyValid = false;
  constructor(@Self() public ngControl: NgControl) {
    // eslint-disable-next-line no-param-reassign
    ngControl.valueAccessor = this;
    this.fileControl.statusChanges
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.fileControl.dirty || this.fileControl.touched)
      )
      .subscribe((status) => {
        this.errorState = status === 'INVALID';
        if (this.ngControl && this.ngControl.control) {
          // handling errors for formControl due to async validators
          this.ngControl.control.setErrors(this.fileControl.errors);
          if (status === 'VALID' && !this.wasPreviouslyValid) {
            // preventing multiple emits of the same value when the formControl is valid
            this.selectedFiles.emit(this.fileControl.value);
          }
          if (status !== 'VALID') {
            this.wasPreviouslyValid = false;
          }
        }
        this.stateChanges.next();
      });
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    if (!this.disabled && (event.target as Element).tagName.toLowerCase() !== 'input') {
      this.inputFileUpload?.nativeElement.focus();
      this.inputFileUpload?.nativeElement.click();
      this.focused = true;
      this.fileControl.markAsTouched();
      if (this.firstCointainerClick) {
        this.fileControl.updateValueAndValidity();
        this.firstCointainerClick = false;
      }
      this.stateChanges.next();
    }
  }

  writeValue(files: File[]): void {
    this.value = files;
    this.fileControl.setValue(files);
  }

  registerOnChange(fn: any): void {
    this.fileControl.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onTouched = () => {};

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  async onFileSelected(event: any) {
    // eslint-disable-next-line prefer-destructuring
    const files: File[] = event.target.files;
    if (files.length > 0) {
      this.fileControl.setValue(files);
      this.fileName = files.length > 1 ? `${files.length} files to upload` : files[0].name;
      this.value = files;
      this.onTouched();
      this.stateChanges.next();
    }
  }

  // component specific Validator
  fileTypeValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const files: File[] = control.value;
      if (!files || files.length === 0) {
        return null;
      }
      // eslint-disable-next-line no-underscore-dangle
      const acceptedTypes = this._accept.split(',').map((type) => type.trim());
      const invalidFiles = [...files].filter(
        (file) => !acceptedTypes.includes(file.type) && !acceptedTypes.includes(`.${file.name.split('.').pop()}`)
      );

      return invalidFiles.length > 0 ? { invalidFileType: true } : null;
    };
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.destroy$.next();
    this.destroy$.complete();
  }
}
