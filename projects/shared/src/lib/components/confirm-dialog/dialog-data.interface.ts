export interface DialogData {
  title: string;
  text: string;
  textArea?: boolean;
  hasActions: boolean;
  type?: 'info' | 'warning' | 'error';
  mode: 'delete' | 'create' | 'update' | 'confirmAction' | 'activate' | 'confirmWithComment';
  itemIdentifier?: string;
  choices?: ChoiceInDialog[];
}

export interface ChoiceInDialog {
  label: string;
  name: string;
  isDefault?: boolean;
}
