import { Component, Inject, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatInputModule } from '@angular/material/input';
import { DialogData } from './dialog-data.interface';
import { TranslocoCoreModule } from '../../transloco/transloco.module';

@Component({
  selector: 'lib-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    TranslocoCoreModule,
    MatRadioModule,
    ReactiveFormsModule,
    TextFieldModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class ConfirmDialogComponent implements OnInit {
  selectedOption: string | undefined;
  form: FormGroup = new FormGroup({
    comment: new FormControl('', Validators.required),
  });

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {
    if (this.data.choices) {
      const result = this.data.choices.find((choice) => choice.isDefault);
      this.selectedOption = result ? result.name : this.selectedOption;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
