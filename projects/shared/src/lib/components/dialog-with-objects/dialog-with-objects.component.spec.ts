import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { DialogWithObjectsComponent } from './dialog-with-objects.component';

describe('DialogWithObjectsComponent', () => {
  let component: DialogWithObjectsComponent;
  let fixture: ComponentFixture<DialogWithObjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DialogWithObjectsComponent],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DialogWithObjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
