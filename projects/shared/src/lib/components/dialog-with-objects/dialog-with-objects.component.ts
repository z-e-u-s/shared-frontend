import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { TranslocoCoreModule } from '../../transloco/transloco.module';
import { DialogWithObjectsData } from './dialog-with-objects-data.interface';

@Component({
  selector: 'lib-dialog-with-objects',
  templateUrl: './dialog-with-objects.component.html',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    TranslocoCoreModule,
    MatListModule,
    MatIconModule,
    RouterModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogWithObjectsComponent {
  dialogRef = inject(MatDialogRef<DialogWithObjectsComponent>);
  data: DialogWithObjectsData = inject(MAT_DIALOG_DATA);

  onNoClick(): void {
    this.dialogRef.close();
  }
}
