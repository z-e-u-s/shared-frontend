import { DialogData } from '../confirm-dialog/dialog-data.interface';

export interface DialogWithObjectsData extends DialogData {
  warningMessage: string;
  relatedObjects: Array<{ id: string; name: string; status?: string }>;
  relatedObjButtonTitle: string;
  routerLink: string;
}
