import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatTooltipHarness } from '@angular/material/tooltip/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';
import { FormFieldComponent } from './form-field.component';
import { MockSvgIconComponent } from '../../../test-data/svg-icon-component';
import { TranslocoCoreModule } from '../../transloco/transloco.module';

describe('FormFieldComponent', () => {
  let component: FormFieldComponent;
  let fixture: ComponentFixture<FormFieldComponent>;
  let loader: HarnessLoader;

  const mockFormControl = new FormControl('', Validators.required);
  const mockInvalidFormControl = new FormControl('', Validators.required);
  const mockIdentifier = 'name';
  const mockLabel = 'Name';
  const mockPlaceholder = 'Enter your name';
  const mockCustomIconPath = '../../test-data/wrench.svg';
  const mockDefaultIconPath = '../../test-data/help.svg';
  const mockDefaultErrorIconPath = '../../test-data/alert.svg';
  const mockTranslatedTooltip = 'Enter your full name please';
  const mockTranslatedErrorMessage = 'This is a required field';

  beforeEach(async () => {
    jest.clearAllMocks();

    await TestBed.configureTestingModule({
      imports: [
        FormFieldComponent,
        CommonModule,
        ReactiveFormsModule,
        getTranslocoTestingModule(),
        MockSvgIconComponent,
      ],
    })
      .overrideComponent(FormFieldComponent, {
        set: {
          imports: [CommonModule, ReactiveFormsModule, MockSvgIconComponent, MatTooltipModule, TranslocoCoreModule],
        },
      })
      .compileComponents();

    fixture = TestBed.createComponent(FormFieldComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.componentRef.setInput('formControl', mockFormControl);
    fixture.componentRef.setInput('identifier', mockIdentifier);
    component.iconPath = mockDefaultIconPath;
    component.errorIconPath = mockDefaultErrorIconPath;

    // Set up an invalid form control
    mockInvalidFormControl.markAsTouched();
    mockInvalidFormControl.setErrors({ required: true });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Input field values', () => {
    it('should update the value of the input field', () => {
      const input = fixture.nativeElement.querySelector('[data-testid="input-name"]');
      const event = new Event('input');
      input.value = 'Jane Roe';

      input.dispatchEvent(event);

      expect(fixture.componentInstance.formControl.value).toEqual('Jane Roe');
    });

    it('should update the value in the control', () => {
      component.formControl.setValue('John Doe');
      fixture.detectChanges();

      const input = fixture.nativeElement.querySelector('[data-testid="input-name"]');

      expect(input.value).toBe('John Doe');
    });
  });

  describe('Input field state', () => {
    it('should disable the input field', () => {
      component.setDisabledState(true);
      fixture.detectChanges();

      const input = fixture.nativeElement.querySelector('[data-testid="input-name"]');

      expect(input.disabled).toBeTruthy();
    });

    it('should enable the input field', () => {
      component.setDisabledState(false);
      fixture.detectChanges();

      const input = fixture.nativeElement.querySelector('[data-testid="input-name"]');

      expect(input.disabled).toBeFalsy();
    });
  });

  describe('Hint text', () => {
    it('should display the hint text "optional" if the input is not required', () => {
      fixture.componentRef.setInput('formControl', new FormControl(''));
      fixture.detectChanges();
      const hintText = fixture.nativeElement.querySelector('[data-testid="input-hint-name"]');

      expect(hintText.textContent).toBe('Optional');
    });

    it('should not display the hint text "optional" if the input is required', () => {
      const hintText = fixture.nativeElement.querySelector('[data-testid="input-hint-name"]');

      expect(hintText).toBeFalsy();
    });
  });

  describe('Input field attributes', () => {
    it('should display the correct label', () => {
      fixture.componentRef.setInput('label', mockLabel);
      fixture.detectChanges();

      const label = fixture.nativeElement.querySelector('[data-testid="input-label-name"]');

      expect(label.textContent).toBe(mockLabel);
    });

    it('should display the correct placeholder', () => {
      fixture.componentRef.setInput('placeholder', mockPlaceholder);
      fixture.detectChanges();

      const input = fixture.nativeElement.querySelector('[data-testid="input-name"]');

      expect(input.placeholder).toBe(mockPlaceholder);
    });
  });

  describe('Hint Icon Attributes', () => {
    it('should not display a hint icon if showIcon is set to false', () => {
      fixture.componentRef.setInput('showIcon', false);
      fixture.detectChanges();

      const icon = fixture.nativeElement.querySelector('[data-testid="input-help-icon-name"]');

      expect(icon).toBeFalsy();
    });

    it('should display the default hint icon if showIcon is set to true and no iconPath is specified', () => {
      fixture.componentRef.setInput('showIcon', true);
      fixture.detectChanges();

      const icon = fixture.nativeElement.querySelector('[data-testid="input-help-icon-name"]');

      expect(icon).toBeTruthy();
      expect(icon.getAttribute('ng-reflect-src')).toBe(mockDefaultIconPath);
    });

    it('should display the specified hint icon if showIcon is set to true and iconPath is specified', () => {
      fixture.componentRef.setInput('showIcon', true);
      fixture.componentRef.setInput('iconPath', mockCustomIconPath);
      fixture.detectChanges();

      const icon = fixture.nativeElement.querySelector('[data-testid="input-help-icon-name"]');

      expect(icon).toBeTruthy();
      expect(icon.getAttribute('ng-reflect-src')).toBe(mockCustomIconPath);
    });
  });

  describe('Error Icon Attributes', () => {
    it('should not display an error icon if the form control has no errors', () => {
      const errorIcon = fixture.nativeElement.querySelector('[data-testid="input-error-icon-name"]');
      expect(errorIcon).toBeFalsy();
    });

    it('should display the default error icon, if the form control is touched and has errors', () => {
      fixture.componentRef.setInput('formControl', mockInvalidFormControl);
      fixture.detectChanges();

      const errorIcon = fixture.nativeElement.querySelector('[data-testid="input-error-icon-name"]');

      expect(errorIcon).toBeTruthy();
      expect(errorIcon.getAttribute('ng-reflect-src')).toBe(mockDefaultErrorIconPath);
    });
  });

  describe('Help Tooltip Attributes', () => {
    it('should be disabled if no translatedTooltip is specified', async () => {
      fixture.componentRef.setInput('showIcon', true);
      fixture.detectChanges();

      const tooltip = await loader.getHarness(MatTooltipHarness.with({ selector: '.mat-mdc-tooltip-trigger' }));
      const isTooltipDisabled = await tooltip.isDisabled();

      expect(isTooltipDisabled).toBe(true);
    });

    it('should render help tooltip with the specified text', async () => {
      fixture.componentRef.setInput('showIcon', true);
      fixture.componentRef.setInput('translatedTooltip', mockTranslatedTooltip);
      fixture.detectChanges();

      const tooltip = await loader.getHarness(MatTooltipHarness.with({ selector: '.mat-mdc-tooltip-trigger' }));

      expect(tooltip).toBeTruthy();

      await tooltip.show();

      const isTooltipOpen = await tooltip.isOpen();

      expect(isTooltipOpen).toBe(true);

      const tooltipText = await tooltip.getTooltipText();

      expect(tooltipText).toEqual(mockTranslatedTooltip);
    });
  });

  describe('Error Tooltip Attributes', () => {
    it('should be disabled if no translatedErrorMessage is specified', async () => {
      fixture.componentRef.setInput('formControl', mockInvalidFormControl);
      fixture.detectChanges();

      const tooltip = await loader.getHarness(MatTooltipHarness.with({ selector: '.mat-mdc-tooltip-trigger' }));
      const isTooltipDisabled = await tooltip.isDisabled();

      expect(isTooltipDisabled).toBe(true);
    });

    it('should render error message with the specified text', async () => {
      fixture.componentRef.setInput('translatedErrorMessage', mockTranslatedErrorMessage);
      fixture.componentRef.setInput('formControl', mockInvalidFormControl);
      fixture.detectChanges();

      const tooltip = await loader.getHarness(MatTooltipHarness.with({ selector: '.mat-mdc-tooltip-trigger' }));

      expect(tooltip).toBeTruthy();

      await tooltip.show();

      const isTooltipOpen = await tooltip.isOpen();

      expect(isTooltipOpen).toBe(true);

      const tooltipText = await tooltip.getTooltipText();

      expect(tooltipText).toEqual(mockTranslatedErrorMessage);
    });
  });
});
