import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, forwardRef, inject, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, ReactiveFormsModule, Validators } from '@angular/forms';
import { SvgIconComponent } from 'angular-svg-icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { TranslocoCoreModule } from '../../transloco/transloco.module';

@Component({
  selector: 'lib-form-field',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, SvgIconComponent, MatTooltipModule, TranslocoCoreModule],
  templateUrl: './form-field.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormFieldComponent),
      multi: true,
    },
  ],
})
export class FormFieldComponent implements OnInit, ControlValueAccessor {
  private readonly destroyRef = inject(DestroyRef);

  @Input({ required: true }) formControl!: FormControl;
  /** The identifier property will be used as:
   * - name attribute for the input field
   * - id attribute for the input field
   * - for attribute for the label
   * - data-testid attribute with different selectors troughout the template
   */
  @Input({ required: true }) identifier!: string;
  /** Make sure the input type matches the validators of the formControl and vice versa */
  @Input() type: 'text' | 'email' | 'number' = 'text';
  @Input() label: string = '';
  @Input() showIcon: boolean = false;
  @Input() iconPath: string = 'assets/icons/tablericons/help-circle-filled.svg';
  @Input() errorIconPath: string = 'assets/icons/tablericons/alert-circle-filled.svg';
  @Input() translatedTooltip: string = '';
  @Input() translatedErrorMessage: string = '';
  @Input() placeholder: string = '';

  protected readonly validators = Validators;
  private onChange: (value: any) => void = () => {};
  private onTouched: () => void = () => {};

  ngOnInit(): void {
    this.formControl.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef), debounceTime(150), distinctUntilChanged())
      .subscribe((value) => {
        this.onChange(value);
        this.onTouched();
      });
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.formControl.setValue(value);
    }
  }

  registerOnChange(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.formControl.disable();
    } else {
      this.formControl.enable();
    }
  }
}
