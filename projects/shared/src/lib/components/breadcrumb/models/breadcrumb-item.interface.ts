import { HashMap } from '@jsverse/transloco';

export interface BreadcrumbItem {
  label: string;
  translate?: boolean;
  queryParams?: HashMap;
  messageParams?: HashMap;
  routerLink?: any[] | string;
  fragment?: string;
  icon?: string;
}
