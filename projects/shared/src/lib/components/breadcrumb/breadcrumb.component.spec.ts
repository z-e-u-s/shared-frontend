import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatIconModule } from '@angular/material/icon';
import { MockModule } from 'ng-mocks';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';
import { MOCK_BREADCRUMB_ITEMS } from '../../../test-data/breadcrumb';
import { BreadcrumbComponent } from './breadcrumb.component';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BreadcrumbComponent, RouterTestingModule, getTranslocoTestingModule(), MockModule(MatIconModule)],
    }).compileComponents();

    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    component.breadcrumbItems.set(MOCK_BREADCRUMB_ITEMS);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find breadcrumb items on the view when breadcrumbItems is not empty', () => {
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('#breadcrumb')).toBeTruthy();
    expect(compiled.querySelector('a')).toBeTruthy();
  });
});
