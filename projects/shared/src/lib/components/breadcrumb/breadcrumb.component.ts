import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { TranslocoCoreModule } from '../../transloco/transloco.module';
import { BreadcrumbItem } from './models/breadcrumb-item.interface';

/** Use this signal to initialize breadcrumbItems in a component whenever needed.
 * Make sure to clear the signal when the component is destroyed, by using .set([]).
 */
export const BREADCRUMB_ITEMS = signal<BreadcrumbItem[]>([]);

@Component({
  selector: 'lib-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TranslocoCoreModule, RouterModule, MatIconModule],
})
export class BreadcrumbComponent {
  breadcrumbItems = BREADCRUMB_ITEMS;
}
