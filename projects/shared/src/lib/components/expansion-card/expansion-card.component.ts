import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { CommonModule } from '@angular/common';
import { TranslocoModule } from '@jsverse/transloco';
import { UserIconAvatarComponent } from '../user-icon-avatar/user-icon-avatar.component';

@Component({
  selector: 'lib-expansion-card',
  templateUrl: './expansion-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, TranslocoModule, MatExpansionModule, UserIconAvatarComponent],
})
export class ExpansionCardComponent {
  @Input({ required: true }) title!: string;
  @Input() author?: string | undefined;
  @Input() id?: string | undefined;
  @Input() expanded = false;
  @Output() toggleExpand = new EventEmitter<void>();
  @Output() opened = new EventEmitter<void>();
}
