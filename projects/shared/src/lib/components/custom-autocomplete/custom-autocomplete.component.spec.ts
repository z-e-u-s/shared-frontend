import { OverlayModule } from '@angular/cdk/overlay';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatSort } from '@angular/material/sort';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SvgIconComponent } from 'angular-svg-icon';
import { BehaviorSubject, of } from 'rxjs';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';
import { DynamicTableComponent } from '../dynamic-table/dynamic-table.component';
import { CustomAutocompleteComponent } from './custom-autocomplete.component';

interface TestOption {
  id: number;
  name: string;
  description: string;
}

describe('CustomAutocompleteComponent', () => {
  let component: CustomAutocompleteComponent<TestOption>;
  let fixture: ComponentFixture<CustomAutocompleteComponent<TestOption>>;
  const mockSort: MatSort = {} as MatSort;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CustomAutocompleteComponent,
        DynamicTableComponent,
        getTranslocoTestingModule(),
        NoopAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatInputModule,
        OverlayModule,
        SvgIconComponent,
      ],
      providers: [{ provide: MatSort, useValue: mockSort }],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomAutocompleteComponent<TestOption>);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should focus the input field on initialization if hasFocus is true', () => {
    component.hasFocus = true;
    jest.spyOn(component.searchInputField?.nativeElement, 'focus');
    component.ngAfterViewInit();
    expect(component.searchInputField?.nativeElement.focus).toHaveBeenCalled();
  });

  it('should not focus the input field on initialization if hasFocus is false', () => {
    component.hasFocus = false;
    jest.spyOn(component.searchInputField?.nativeElement, 'focus');
    component.ngAfterViewInit();
    expect(component.searchInputField?.nativeElement.focus).not.toHaveBeenCalled();
  });

  it('should focus the input field when hasFocus changes from false to true', () => {
    component.hasFocus = false;
    jest.spyOn(component.searchInputField?.nativeElement, 'focus');
    component.ngOnChanges({
      hasFocus: { currentValue: true, previousValue: false, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchInputField?.nativeElement.focus).toHaveBeenCalled();
  });

  it('should not focus the input field when hasFocus changes from true to false', () => {
    component.hasFocus = true;
    jest.spyOn(component.searchInputField?.nativeElement, 'focus');
    component.ngOnChanges({
      hasFocus: { currentValue: false, previousValue: true, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchInputField?.nativeElement.focus).not.toHaveBeenCalled();
  });

  it('should call setValidators() and updateValueAndValidity() when required changes to true', () => {
    jest.spyOn(component.searchFormControl, 'setValidators');
    jest.spyOn(component.searchFormControl, 'updateValueAndValidity');
    component.required = true;
    component.ngOnChanges({
      required: { currentValue: true, previousValue: false, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchFormControl.setValidators).toHaveBeenCalledTimes(1);
    expect(component.searchFormControl.updateValueAndValidity).toHaveBeenCalledTimes(1);
  });

  it('should call addValidators() and updateValueAndValidity() when required changes to true', () => {
    jest.spyOn(component.searchFormControl, 'addValidators');
    jest.spyOn(component.searchFormControl, 'updateValueAndValidity');
    component.required = true;
    component.ngOnChanges({
      required: { currentValue: true, previousValue: false, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchFormControl.addValidators).toHaveBeenCalledTimes(1);
    expect(component.searchFormControl.updateValueAndValidity).toHaveBeenCalledTimes(1);
  });

  it('should call removeValidators() and updateValueAndValidity() when required changes to false', () => {
    jest.spyOn(component.searchFormControl, 'removeValidators');
    jest.spyOn(component.searchFormControl, 'updateValueAndValidity');
    component.required = false;
    component.ngOnChanges({
      required: { currentValue: false, previousValue: true, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchFormControl.removeValidators).toHaveBeenCalledTimes(1);
    expect(component.searchFormControl.updateValueAndValidity).toHaveBeenCalledTimes(1);
  });

  it('should call addValidators() and updateValueAndValidity() when isFieldNameEcsField changes to true', () => {
    jest.spyOn(component.searchFormControl, 'addValidators');
    jest.spyOn(component.searchFormControl, 'updateValueAndValidity');
    component.isFieldNameEcsField = true;
    component.ngOnChanges({
      isFieldNameEcsField: { currentValue: true, previousValue: false, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchFormControl.addValidators).toHaveBeenCalledTimes(1);
    expect(component.searchFormControl.updateValueAndValidity).toHaveBeenCalledTimes(1);
  });

  it('should call removeValidators() and updateValueAndValidity() when isFieldNameEcsField changes to false', () => {
    jest.spyOn(component.searchFormControl, 'removeValidators');
    jest.spyOn(component.searchFormControl, 'updateValueAndValidity');
    component.isFieldNameEcsField = false;
    component.ngOnChanges({
      isFieldNameEcsField: { currentValue: false, previousValue: true, firstChange: false, isFirstChange: () => false },
    });
    expect(component.searchFormControl.removeValidators).toHaveBeenCalledTimes(1);
    expect(component.searchFormControl.updateValueAndValidity).toHaveBeenCalledTimes(1);
  });

  it('should update dataSource and not change lazyDropdownOptions$ when dropdownOptions changes and useLazyLoad = false', () => {
    component.useLazyLoad = false;
    component.lazyDropdownOptions$ = new BehaviorSubject([]);
    component.dataSource.data = [];
    component.dropdownOptions = [{ id: 1, name: 'test', description: 'test' }];
    component.ngOnChanges({
      dropdownOptions: {
        currentValue: [{ id: 1, name: 'test', description: 'test' }],
        previousValue: [],
        firstChange: false,
        isFirstChange: () => false,
      },
    });
    expect(component.dataSource.data).toEqual([{ id: 1, name: 'test', description: 'test' }]);
    expect(component.lazyDropdownOptions$).toEqual(new BehaviorSubject([]));
    component.lazyDropdownOptions$.subscribe((options) => {
      expect(options).toEqual([]);
    });
  });

  it('should update lazyDropdownOptions$ and not change dataSource when dropdownOptions changes and useLazyLoad = true', () => {
    component.useLazyLoad = true;
    component.lazyDropdownOptions$ = new BehaviorSubject([]);
    component.dataSource.data = [];
    component.dropdownOptions = [{ id: 1, name: 'test', description: 'test' }];
    component.ngOnChanges({
      dropdownOptions: {
        currentValue: [{ id: 1, name: 'test', description: 'test' }],
        previousValue: [],
        firstChange: false,
        isFirstChange: () => false,
      },
    });
    expect(component.dataSource.data).toEqual([]);
    expect(component.lazyDropdownOptions$).not.toEqual(new BehaviorSubject([]));
    component.lazyDropdownOptions$.subscribe((options) => {
      expect(options).toEqual([{ id: 1, name: 'test', description: 'test' }]);
    });
  });

  it('should write value when calling writeValue() with a valid argument', () => {
    jest.spyOn(component.searchFormControl, 'setValue');
    expect(component.searchFormControl).toBeTruthy();
    expect(component.searchFormControl.value).toEqual('');

    component.writeValue({ id: 1, name: 'test', description: 'test' });
    expect(component.searchFormControl.setValue).toHaveBeenCalledWith({ id: 1, name: 'test', description: 'test' });
  });

  it('should not write value when calling writeValue() with an invalid argument', () => {
    component.searchFormControl = new FormControl('');
    expect(component.searchFormControl).toBeTruthy();
    expect(component.searchFormControl.value).toEqual('');

    component.writeValue(undefined);
    expect(component.searchFormControl.value).toEqual('');
  });

  it('should disable searchFormControl when calling setDisabledState(true)', () => {
    jest.spyOn(component.searchFormControl, 'disable');
    jest.spyOn(component.searchFormControl, 'enable');
    component.setDisabledState(true);
    expect(component.searchFormControl.enable).not.toHaveBeenCalled();
    expect(component.searchFormControl.disable).toHaveBeenCalled();
    expect(component.searchFormControl.disabled).toEqual(true);
  });

  it('should enable searchFormControl when calling setDisabledState(false)', () => {
    jest.spyOn(component.searchFormControl, 'enable');
    component.setDisabledState(false);
    expect(component.searchFormControl.enable).toHaveBeenCalled();
    expect(component.searchFormControl.enabled).toEqual(true);
  });

  it('should call onSearchQueryChanged when calling ngOnInit()', () => {
    jest.spyOn(component, 'onSearchQueryChanged');
    jest.spyOn(component.lazyDropdownOptions$, 'pipe').mockReturnValueOnce(of({ subscribe: () => {} }));

    component.lazyDropdownOptions$ = new BehaviorSubject([]);
    component.ngOnInit();

    expect(component.onSearchQueryChanged).toHaveBeenCalled();
  });
});
