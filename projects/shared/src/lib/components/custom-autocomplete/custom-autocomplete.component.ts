import { CdkConnectedOverlay, CdkOverlayOrigin, ConnectedPosition } from '@angular/cdk/overlay';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DestroyRef,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener,
  inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatError, MatFormField, MatHint, MatLabel, MatPrefix, MatSuffix } from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatInput } from '@angular/material/input';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTooltip } from '@angular/material/tooltip';
import { TranslocoPipe } from '@jsverse/transloco';
import { debounceTime, delay, distinctUntilChanged, Observable, of, Subject, takeUntil } from 'rxjs';
import { invalidLogprepCharsValidator } from '../../form-field-validator/invalid-logprep-chars.validator';
import { lazyArray } from '../../helpers/lazy-array.helper';
import { ColumnDefinition } from '../dynamic-table/column-definition.model';
import { DynamicTableComponent } from '../dynamic-table/dynamic-table.component';

@Component({
  selector: 'lib-custom-autocomplete',
  templateUrl: './custom-autocomplete.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomAutocompleteComponent),
      multi: true,
    },
  ],
  standalone: true,
  imports: [
    MatFormField,
    MatLabel,
    MatInput,
    ReactiveFormsModule,
    CdkOverlayOrigin,
    MatIcon,
    MatSuffix,
    MatTooltip,
    MatPrefix,
    MatError,
    MatHint,
    CdkConnectedOverlay,
    DynamicTableComponent,
    TranslocoPipe,
  ],
})
export class CustomAutocompleteComponent<T>
  implements ControlValueAccessor, OnInit, OnChanges, AfterViewInit, OnDestroy
{
  private destroyRef = inject(DestroyRef);
  @Input({ required: true }) displayColumnDefs!: ColumnDefinition[];
  @Input({ required: true }) searchLabel!: string;
  @Input({ required: true }) searchColumn!: string | number;
  @Input() filterableColumns?: string[];
  @Input({ required: true }) dropdownOptions!: T[];
  @Input() defaultSortingName?: string;
  @Input() useLazyLoad?: boolean = false;
  @Input() overlayHasFocus = true;
  @Input() required?: boolean = false;
  @Input() hasFocus?: boolean = false;
  @Input() hasCancelButton?: boolean = false;
  @Input() hintMessage?: string;
  @Input() errorMessage?: string;
  @Input() showValueIsNotInDropdownOptionsWarning: boolean = false;
  @Input() isFieldNameEcsField: boolean = false;
  @Output() optionSelected = new EventEmitter<T>();
  @Output() blurSearchField = new EventEmitter<void>();
  @Output() searchQueryChanged = new EventEmitter<string>();

  searchFormControl: FormControl<any> = new FormControl('');
  currentOption: T | undefined;
  lazyDropdownOptions$: Observable<T[]> = new Subject();

  @ViewChild('searchInputField', { read: ElementRef }) searchInputField: ElementRef | undefined;
  @ViewChild('sort', { static: true }) sort: MatSort | undefined;

  dataSource = new MatTableDataSource<T>([]);
  highlightRowIndex = -1;
  isOverlayOpen = false;
  overlayPositions: ConnectedPosition[] = [
    { originX: 'start', originY: 'bottom', overlayX: 'start', overlayY: 'top' },
    { originX: 'start', originY: 'top', overlayX: 'start', overlayY: 'bottom' },
    { originX: 'end', originY: 'bottom', overlayX: 'end', overlayY: 'top' },
    { originX: 'end', originY: 'top', overlayX: 'end', overlayY: 'bottom' },
  ];
  /**
   * Declare a subject to signal whether a subscription to lazy loading should be canceled
   */
  private cancelLazyLoad$ = new Subject<void>();

  constructor(private cdRef: ChangeDetectorRef) {}

  private onChange: (value: any) => void = () => {};
  private onTouched: () => void = () => {};

  writeValue(value: any): void {
    if (value !== undefined) {
      this.searchFormControl.setValue(value);
    }
  }

  registerOnChange(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.searchFormControl.disable();
    } else {
      this.searchFormControl.enable();
    }
  }

  /**
   * In this lyfecycle hook, we set the sort for the dataSource and call the onSearchQueryChanged method in order to
   * perform the appropriate logic when the search query changes.
   */
  ngOnInit(): void {
    this.onSearchQueryChanged();
  }

  /**
   * In this lyfecycle hook, we set the focus on the searchInputField if the hasFocus input is true.
   */
  ngAfterViewInit(): void {
    if (this.hasFocus) {
      this.searchInputField?.nativeElement.focus();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['hasFocus']?.currentValue && this.searchInputField) {
      this.searchInputField.nativeElement.focus();
      this.onFocusSearchField();
    }
    if (changes['required']) {
      if (this.required) {
        this.searchFormControl.addValidators([Validators.required]);
      } else {
        this.searchFormControl.removeValidators([Validators.required]);
      }
      this.searchFormControl.updateValueAndValidity();
    }

    if (changes['isFieldNameEcsField']) {
      if (this.isFieldNameEcsField) {
        this.searchFormControl.addValidators([invalidLogprepCharsValidator()]);
      } else {
        this.searchFormControl.removeValidators([invalidLogprepCharsValidator()]);
      }
      this.searchFormControl.updateValueAndValidity();
    }

    if (changes['dropdownOptions']?.currentValue) {
      if (this.useLazyLoad) {
        this.lazyLoadDropdownOptions(this.dropdownOptions);
      } else {
        this.dataSource = new MatTableDataSource<T>(this.dropdownOptions);
      }
      this.highlightRowIndex = -1;
    }
    if (changes['filterableColumns']?.currentValue?.length) {
      this.dataSource.filterPredicate = (data: T, filter: string) => {
        let dataMatchesFilter = false;
        const filterText = filter.trim().toLowerCase();
        this.filterableColumns?.forEach((field) => {
          dataMatchesFilter = dataMatchesFilter || (data as any)[field].toLowerCase().includes(filterText);
        });
        return dataMatchesFilter;
      };
    }
  }

  /**
   * The goal here is to automatically show the drop-down (the overlay with list of options) when user sets the focus in the search-field.
   * Since the drop-down (overlay) is configured this way: [cdkConnectedOverlayOpen]="isOverlayOpen",
   * we need to set isOverlayOpen to true to open it.
   * So when dropdownOptions is not empty and the search-field searchFormControl has no value yet, and user sets focus in it,
   * it will show up the list of options just like any other standard mat-select or mat-autocomplete.
   */
  onFocusSearchField(): void {
    this.isOverlayOpen = !!(this.dropdownOptions && this.dropdownOptions?.length && !this.searchFormControl?.value);
  }

  /**
   * This method is used to perform the appropriate logic when the user clicks on the remove icon in the search field.
   * @param event
   */
  onClickRemoveSearchQuery(event: MouseEvent): void {
    event.stopPropagation();
    this.searchFormControl.setValue('');
    this.onChange('');
    this.onTouched();
    this.applyFilter('');
  }

  /**
   * This method is used to perform the appropriate logic when the search query changes in the search field.
   */
  onSearchQueryChanged(): void {
    this.searchFormControl?.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        debounceTime(150), // debounce searchQueries & dismiss consecutive duplicates
        distinctUntilChanged()
      )
      .subscribe((searchQuery) => {
        this.onChange(searchQuery);
        this.onTouched();
        this.applyFilter(searchQuery);
        this.searchQueryChanged.emit(searchQuery);
      });
  }

  /**
   * This method filters the original list based on the search query.
   * It checks whether the lazy loading is enabled or not:
   * - If it is, it will load the filtered list in chunks.
   * - If it is not, it will load the whole filtered list.
   * @param searchQuery
   */
  applyFilter(searchQuery: string): void {
    if (this.useLazyLoad) {
      // Filter the original list and load it progressively in chunks
      const filteredOptions = this.dropdownOptions?.filter((item) => {
        let dataMatchesFilter = false;
        const filterText = searchQuery.trim().toLowerCase();

        this.filterableColumns?.forEach((field) => {
          dataMatchesFilter = dataMatchesFilter || (item as any)[field].toLowerCase().includes(filterText);
        });
        return dataMatchesFilter;
      });

      this.lazyLoadDropdownOptions(filteredOptions);
    } else {
      // load the whole filtered list
      this.dataSource.filter = searchQuery?.trim().toLowerCase();
      this.cdRef.markForCheck();
    }
  }

  /**
   * This method is used to load the dropdownOptions in chunks, to avoid freezing the UI when the dropdownOptions is too big.
   * @param dropdownOptions
   */
  lazyLoadDropdownOptions(dropdownOptions: T[] | undefined): void {
    // Cancel the previous lazy loading subscription
    this.cancelLazyLoad$.next();

    if (dropdownOptions?.length) {
      // Update and resubscribe to the lazyDropdownOptions$ observable
      // @ts-ignore
      this.lazyDropdownOptions$ = of(dropdownOptions).pipe(lazyArray(100, 50)); // load 50 items every 100ms

      // Subscribe to the new lazyDropdownOptions$
      this.lazyDropdownOptions$
        .pipe(takeUntil(this.cancelLazyLoad$), takeUntilDestroyed(this.destroyRef))
        .subscribe((list) => {
          this.dataSource.data = list;
          this.cdRef.markForCheck();
        });
    } else {
      this.dataSource.data = [];
      this.cdRef.markForCheck();
    }
  }

  /**
   * Navigate between options in overlay and highlight the one that has keydown.arrowdown
   */
  @HostListener('keydown.arrowdown', ['$event'])
  onArrowDown(): void {
    if (this.overlayHasFocus && this.highlightRowIndex < this.dataSource.filteredData.length - 1) {
      this.highlightRowIndex += 1;
    }
  }

  /**
   * Navigate between options in overlay and highlight the one that has keydown.arrowup
   */
  @HostListener('keydown.arrowup', ['$event'])
  onArrowUp(): void {
    if (this.overlayHasFocus && this.highlightRowIndex > 0) {
      this.highlightRowIndex -= 1;
    }
  }

  /**
   * Select an option when user presses the ENTER key and the overlay is open. If the overlay is not open, open it.
   * @param event
   */
  @HostListener('keydown.enter', ['$event'])
  onKeyEnter(event: KeyboardEvent): void {
    event.preventDefault();
    if (this.isOverlayOpen) {
      if (
        this.overlayHasFocus &&
        this.highlightRowIndex >= 0 &&
        this.highlightRowIndex < this.dataSource.filteredData.length
      ) {
        // data
        this.selectRow(this.dataSource.filteredData[this.highlightRowIndex]);
      }
    } else {
      this.openOverlay();
    }
  }

  /**
   * open searchList when user is typing
   * @param event
   */
  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    if (event.key !== 'Enter' && event.key !== 'Tab' && !this.isOverlayOpen) {
      this.openOverlay();
    }
  }

  /**
   * Select an option when user clicks on it and close the overlay if the parameter 'closeOverlay' is true.
   * @param row
   * @param closeOverlay
   */
  selectRow(row: T, closeOverlay = true): void {
    if (closeOverlay) {
      this.closeOverlay();
    }
    this.currentOption = row;
    this.searchFormControl?.setValue((row as any)[this.searchColumn!]);
    this.onChange((row as any)[this.searchColumn!]);
    this.onTouched();
    this.optionSelected.emit(row);
    if (this.hasFocus) {
      // without the following line, the input field looses the focus after the user clicks on an option
      this.searchInputField?.nativeElement.focus();
    }
  }

  openOverlay(): void {
    this.isOverlayOpen = true;
    this.highlightRowIndex = -1;
  }

  closeOverlay(): void {
    this.isOverlayOpen = false;
    this.highlightRowIndex = -1;
  }

  onDetachOverlay(): void {
    this.closeOverlay();
  }

  /**
   * Delay emit blur event, to consider when overlay is open and user wants to click on an option.
   * In that case, wait till an option is selected & overlay will be autocomplete closed.
   */
  onBlurSearchField() {
    of(null)
      .pipe(delay(100), takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        this.blurSearchField.emit();
      });
  }

  /**
   * Unsubscribe from the lazy loading observable
   */
  ngOnDestroy() {
    this.cancelLazyLoad$.next();
    this.cancelLazyLoad$.complete();
  }
}
