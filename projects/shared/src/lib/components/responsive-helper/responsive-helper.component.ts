import { Component, Inject } from '@angular/core';

import { APP_INFO } from '../../tokens/app-info.token';
import { AppInfo } from '../../models/app-info.interface';

@Component({
  selector: 'lib-responsive-helper',
  templateUrl: './responsive-helper.component.html',
  standalone: true,
  imports: [],
})
export class ResponsiveHelperComponent {
  constructor(@Inject(APP_INFO) public appInfo: AppInfo) {}
}
