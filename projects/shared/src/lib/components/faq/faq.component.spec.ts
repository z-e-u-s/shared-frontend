import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq.component';
import { Faq } from './models/faq.interface';

describe('FaqComponent', () => {
  let component: FaqComponent;
  let fixture: ComponentFixture<FaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommonModule, FaqComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(FaqComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the list of FAQs when faqs input is provided', () => {
    const testFaqs: Faq[] = [
      { question: 'What is Angular?', answer: 'Angular is a framework.' },
      { question: 'What is TypeScript?', answer: 'TypeScript is a superset of JavaScript.' },
    ];
    component.faqs = testFaqs;
    fixture.detectChanges();

    const faqItems = fixture.nativeElement.querySelectorAll('#faq-item');

    expect(faqItems.length).toBe(2);
    expect(faqItems[0].textContent).toContain('What is Angular?');
    expect(faqItems[0].textContent).toContain('Angular is a framework.');
    expect(faqItems[1].textContent).toContain('What is TypeScript?');
    expect(faqItems[1].textContent).toContain('TypeScript is a superset of JavaScript.');
  });

  it('should display "No FAQs found" when faqs input is null', () => {
    component.faqs = null;
    fixture.detectChanges();

    const faqContainer = fixture.nativeElement.querySelector('.container');
    expect(faqContainer.textContent).toContain('No FAQs found');
  });
});
