import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BREADCRUMB_ITEMS } from '../breadcrumb/breadcrumb.component';
import { Faq } from './models/faq.interface';

@Component({
  selector: 'lib-faq',
  templateUrl: './faq.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class FaqComponent implements OnInit {
  @Input() faqs!: Faq[] | null;
  ngOnInit(): void {
    BREADCRUMB_ITEMS.set([{ label: 'FAQ', routerLink: '/faq' }]);
  }
}
