import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MockProvider } from 'ng-mocks';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';
import { AlertService } from './alert.service';

describe('AlertService', () => {
  let service: AlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [getTranslocoTestingModule()],
      providers: [MockProvider(MatSnackBar)],
    });
    service = TestBed.inject(AlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return "done" when calling getIconByMessageType() with "success" type', () => {
    const result = service['getIconByMessageType']('success');
    expect(result).toBe('done');
  });

  it('should return "warning" when calling getIconByMessageType() with "error" type', () => {
    const result = service['getIconByMessageType']('error');
    expect(result).toBe('warning');
  });

  it('should return "warning" when calling getIconByMessageType() with "warning" type', () => {
    const result = service['getIconByMessageType']('warning');
    expect(result).toBe('warning');
  });

  it('should return "info" when calling getIconByMessageType() with any other type', () => {
    // @ts-ignore
    const result = service['getIconByMessageType']('other');
    expect(result).toBe('info');
  });
});
