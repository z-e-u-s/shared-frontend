import { inject, Injectable } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { HashMap, TranslocoService } from '@jsverse/transloco';
import { AlertType } from './alert.types';
import { SnackBarData } from './alert-snack-bar/snack-bar-data.interface';
import { AlertAppearance } from './alert-appearance.types';
import { AlertSnackBarComponent } from './alert-snack-bar/alert-snack-bar.component';

/**
 * This is a custom service to show alerts to the user.
 * The alerts could be API responses, or any other message that we want to show to the user.
 */
@Injectable({ providedIn: 'root' })
export class AlertService {
  private snackBar = inject(MatSnackBar);
  private translateService = inject(TranslocoService);

  openSnackBar(
    message: string,
    messageParams?: HashMap,
    type: AlertType = 'info',
    translate: boolean = false,
    duration: number = 5000,
    appearance: AlertAppearance = 'fill',
    verticalPosition: MatSnackBarVerticalPosition = 'top',
    horizontalPosition: MatSnackBarHorizontalPosition = 'center'
  ) {
    let translatedMessage = message;
    if (translate) {
      translatedMessage = this.translateService.translate(message, messageParams);
    }
    const snackBarData: SnackBarData = {
      message: translatedMessage,
      icon: this.getIconByMessageType(type),
    };

    const snackBarConfig: MatSnackBarConfig = {
      data: snackBarData,
      panelClass: [`alert-type-${appearance}-${type}`],
      verticalPosition, // 'top' | 'bottom'
      horizontalPosition, // 'start' | 'center' | 'end' | 'left' | 'right'
    };
    if (type !== 'error' && type !== 'warning') {
      snackBarConfig.duration = duration;
    }

    this.snackBar.openFromComponent(AlertSnackBarComponent, snackBarConfig);
  }

  private getIconByMessageType(type: AlertType): string {
    switch (type) {
      case 'success':
        return 'done';
      case 'error':
      case 'warning':
        return 'warning';
      default:
        return 'info';
    }
  }
}
