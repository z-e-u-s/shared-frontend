import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import { MockModule, MockProvider } from 'ng-mocks';
import { getTranslocoTestingModule } from '../../../transloco/transloco-testing.module';
import { AlertSnackBarComponent } from './alert-snack-bar.component';

describe('AlertSnackBarComponent', () => {
  let component: AlertSnackBarComponent;
  let fixture: ComponentFixture<AlertSnackBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AlertSnackBarComponent,
        getTranslocoTestingModule(),
        MockModule(MatIconModule),
        MockModule(MatMenuModule),
      ],
      providers: [MockProvider(MatSnackBarRef), { provide: MAT_SNACK_BAR_DATA, useValue: {} }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dismissWithAction() when calling closeSnackbar()', () => {
    jest.spyOn(component.snackBarRef, 'dismissWithAction');
    component.closeSnackbar();
    expect(component.snackBarRef.dismissWithAction).toBeCalledTimes(1);
  });
});
