import { ChangeDetectionStrategy, Component, inject, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarModule, MatSnackBarRef } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SnackBarData } from './snack-bar-data.interface';

/**
 * This is a custom snackbar component to show alerts (or API responses) to the user.
 */
@Component({
  selector: 'lib-alert-snack-bar',
  templateUrl: './alert-snack-bar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [MatIconModule, MatButtonModule, MatSnackBarModule],
})
export class AlertSnackBarComponent {
  snackBarRef = inject(MatSnackBarRef);

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: SnackBarData) {}

  closeSnackbar(): void {
    this.snackBarRef.dismissWithAction();
  }
}
