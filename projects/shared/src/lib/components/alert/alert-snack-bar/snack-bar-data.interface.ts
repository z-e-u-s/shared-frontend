/**
 * Interface for the data that is passed to the snack bar component.
 */
export interface SnackBarData {
  message: string;
  icon?: string;
}
