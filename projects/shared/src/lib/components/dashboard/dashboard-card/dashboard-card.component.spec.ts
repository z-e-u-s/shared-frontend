import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockProvider } from 'ng-mocks';
import { DashboardCardComponent } from './dashboard-card.component';
import { LoadingService } from '../../../services/loading-service/loading.service';

describe('DashboardCardComponent', () => {
  let component: DashboardCardComponent;
  let fixture: ComponentFixture<DashboardCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DashboardCardComponent],
      providers: [MockProvider(LoadingService)],
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardCardComponent);
    component = fixture.componentInstance;
    component.kpiValue = 123;
    component.kpiTitle = 'Number of Detection Rules';
    component.color = 'text-green-500';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
