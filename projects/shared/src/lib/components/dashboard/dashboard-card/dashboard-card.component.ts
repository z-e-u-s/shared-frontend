import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { NgClass, AsyncPipe } from '@angular/common';
import { LoadingService } from '../../../services/loading-service/loading.service';

/**
 * This is the presenter component for the Dashboard-Card. It handles the UI logic.
 * @example
 * <app-dashboard-card [kpiValue]="kpiValue" [kpiTitle]="kpiTitle" [color]="color"></app-dashboard-card>
 */
@Component({
  selector: 'lib-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgClass, MatProgressSpinner, AsyncPipe],
})
export class DashboardCardComponent {
  private readonly loadingService = inject(LoadingService);
  /**
   * The value of the KPI that would be displayed on the card.
   */
  @Input() kpiValue: number | string | null | undefined;
  /**
   * The KPI's title that would be displayed on the card.
   */
  @Input() kpiTitle!: string;
  /**
   * The color with which we will show the KPI's value and title on the card.
   */
  @Input() color: string = 'text-green-500';

  isLoading = this.loadingService.isLoading;
}
