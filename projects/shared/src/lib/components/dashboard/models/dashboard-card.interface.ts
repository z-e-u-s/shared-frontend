import { NavigationExtras } from '@angular/router';

interface ExtendedNavigationExtras extends NavigationExtras {
  [key: string]: any | any[] | undefined;
}

export interface DashboardCard {
  title: string;
  value: number | string | undefined | null;
  highlightIfNotZero: boolean;
  routerLink?: string | undefined;
  navigationExtras?: ExtendedNavigationExtras;
}

export interface NavigationEvent {
  route: string | undefined;
  navigationExtras?: ExtendedNavigationExtras | undefined;
}
