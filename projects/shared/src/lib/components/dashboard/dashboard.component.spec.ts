import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KeycloakProfile } from 'keycloak-js';
import { MockComponent } from 'ng-mocks';
import { DashboardCardComponent } from './dashboard-card/dashboard-card.component';
import { DashboardComponent } from './dashboard.component';
import { getTranslocoTestingModule } from '../../transloco/transloco-testing.module';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [getTranslocoTestingModule()],
      declarations: [DashboardComponent, MockComponent(DashboardCardComponent)],
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    // Mock data for inputs
    component.userProfile = {} as KeycloakProfile;
    component.dashboardCardTopLeft = { title: 'test', value: 0, highlightIfNotZero: false };
    component.dashboardCardTopRight = { title: 'test', value: 0, highlightIfNotZero: false };
    component.dashboardCardBottomLeft = { title: 'test', value: 0, highlightIfNotZero: false };
    component.dashboardCardBottomRight = { title: 'test', value: 0, highlightIfNotZero: false };

    await fixture.whenStable().then(() => {
      fixture.detectChanges();
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit navigateTo event when called', () => {
    jest.spyOn(component.navigateTo, 'emit');
    const mockNavigateTo = { route: 'mockNavigateTo' };
    component.navigateTo.emit(mockNavigateTo);
    expect(component.navigateTo.emit).toHaveBeenCalledWith(mockNavigateTo);
  });

  it('should emit navigateTo event with query params when called', () => {
    jest.spyOn(component.navigateTo, 'emit');
    const mockNavigateTo = { route: 'mockNavigateTo', queryParameters: { query: 'mockQuery' } };
    component.navigateTo.emit(mockNavigateTo);
    expect(component.navigateTo.emit).toHaveBeenCalledWith(mockNavigateTo);
  });
});
