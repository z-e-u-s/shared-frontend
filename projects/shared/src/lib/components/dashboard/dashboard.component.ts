import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { KeycloakProfile } from 'keycloak-js';
import { TranslocoPipe } from '@jsverse/transloco';
import { DashboardCardComponent } from './dashboard-card/dashboard-card.component';
import { DashboardCard, NavigationEvent } from './models/dashboard-card.interface';

@Component({
  selector: 'lib-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [DashboardCardComponent, TranslocoPipe],
})
export class DashboardComponent {
  @Input() userProfile?: KeycloakProfile | undefined | null;
  @Input({ required: true }) dashboardCardTopLeft!: DashboardCard | null;
  @Input({ required: true }) dashboardCardTopRight!: DashboardCard | null;
  @Input({ required: true }) dashboardCardBottomLeft!: DashboardCard | null;
  @Input({ required: true }) dashboardCardBottomRight!: DashboardCard | null;
  @Output() navigateTo = new EventEmitter<NavigationEvent>();

  onClickDashboardCard(navigationData: NavigationEvent): void {
    this.navigateTo.emit(navigationData);
  }
}
