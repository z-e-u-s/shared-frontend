import { Component, DestroyRef, EventEmitter, inject, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ReactiveFormsModule, UntypedFormControl } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { SvgIconComponent } from 'angular-svg-icon';
import { TranslocoCoreModule } from '../../transloco/transloco.module';

@Component({
  selector: 'lib-search',
  templateUrl: './search.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    TranslocoCoreModule,
    MatInputModule,
    SvgIconComponent,
  ],
})
export class SearchComponent implements OnInit {
  private destroyRef = inject(DestroyRef);
  @Input() debounce = 300;
  @Input() placeholder?: string;

  @Output() searchQuery = new EventEmitter<string>();

  searchControl: UntypedFormControl = new UntypedFormControl();

  ngOnInit(): void {
    // Subscribe to the search field value changes
    this.searchControl.valueChanges
      .pipe(debounceTime(this.debounce), distinctUntilChanged(), takeUntilDestroyed(this.destroyRef))
      .subscribe((value) => this.searchQuery.emit(value));
  }
}
