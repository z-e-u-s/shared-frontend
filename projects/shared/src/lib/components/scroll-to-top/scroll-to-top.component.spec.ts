import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChangeDetectorRef, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { fromEvent, of, Subject } from 'rxjs';
import { MockModule } from 'ng-mocks';
import { ScrollToTopComponent } from './scroll-to-top.component';

describe('ScrollToTopComponent', () => {
  let component: ScrollToTopComponent;
  let fixture: ComponentFixture<ScrollToTopComponent>;

  // Mock dependencies
  const mockDocument: Document = new DOMParser().parseFromString('<html></html>', 'text/html');
  const mockElementRef: ElementRef = {
    nativeElement: document.createElement('div'),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ScrollToTopComponent, MockModule(MatIconModule), MockModule(MatButtonModule)],
      providers: [
        ChangeDetectorRef,
        { provide: DOCUMENT, useValue: mockDocument },
        { provide: ElementRef, useValue: mockElementRef },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ScrollToTopComponent);
    component = fixture.componentInstance;
    component['destroy$'] = new Subject<void>();
    fixture.detectChanges();
  });

  afterEach(() => {
    component['destroy$'].next();
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set elementScrolled to false when scrolling up', () => {
    component.element = mockElementRef.nativeElement;
    // Simulate scrolling
    jest.spyOn(fromEvent(component.element!, 'scroll'), 'pipe').mockReturnValue(of(new Event('scroll')));
    component.element!.scrollTop = 50;
    component.ngOnInit();
    expect(component.elementScrolled).toBe(false);
  });

  xit('should scroll to top of the specified element and emit event', () => {
    component.element = mockElementRef.nativeElement;
    jest.spyOn(component.element!, 'scrollTo').mockReturnValue(undefined);

    component.scrollToTop();

    expect(component.element!.scrollTo).toHaveBeenCalledWith(0, 0);
    expect(component.scrollToTopClicked.emit).toHaveBeenCalled();
  });

  xit('should scroll to top of the window and emit event', () => {
    jest.spyOn(window, 'scrollTo').mockReturnValue(undefined);

    component.scrollToTop();

    expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
    expect(component.scrollToTopClicked.emit).toHaveBeenCalled();
  });
});
