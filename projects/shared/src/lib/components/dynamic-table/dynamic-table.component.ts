import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe, DecimalPipe, NgClass } from '@angular/common';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ContentChildren,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';
import { MatSort, MatSortHeader } from '@angular/material/sort';
import {
  MatCell,
  MatCellDef,
  MatColumnDef,
  MatHeaderCell,
  MatHeaderCellDef,
  MatHeaderRow,
  MatHeaderRowDef,
  MatNoDataRow,
  MatRow,
  MatRowDef,
  MatTable,
  MatTableDataSource,
} from '@angular/material/table';
import { ColumnDefinition } from './column-definition.model';

@Component({
  selector: 'lib-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    MatTable,
    MatSort,
    MatColumnDef,
    MatHeaderCellDef,
    MatHeaderCell,
    MatSortHeader,
    MatCellDef,
    MatCell,
    NgClass,
    MatHeaderRowDef,
    MatHeaderRow,
    MatRowDef,
    MatRow,
    DecimalPipe,
    DatePipe,
  ],
})
export class DynamicTableComponent<T> implements OnInit, OnChanges, AfterContentInit {
  @Input() highlightRowIndex = -1;

  @Input() displayColumnDefs: ColumnDefinition[] = [];
  @Input() dataList: T[] | undefined;
  @Input() defaultSortingName: string | undefined;
  @Output() rowSelected = new EventEmitter<T>();
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource<T>([]);
  selection: SelectionModel<T> | undefined;

  @ContentChildren(MatHeaderRowDef) headerRowDefs: QueryList<MatHeaderRowDef> | undefined;
  @ContentChildren(MatRowDef) rowDefs: QueryList<MatRowDef<T>> | undefined;
  @ContentChildren(MatColumnDef) columnDefs: QueryList<MatColumnDef> | undefined;
  @ContentChild(MatNoDataRow) noDataRow: MatNoDataRow | undefined;
  @ViewChild(MatSort, { static: true }) sort: MatSort | null = null;

  @ViewChild(MatTable, { static: true }) table: MatTable<T> | undefined;
  @ViewChildren('matrow', { read: ViewContainerRef }) rows: QueryList<ViewContainerRef> | undefined;

  ngAfterContentInit(): void {
    this.columnDefs?.forEach((columnDef) => this.table?.addColumnDef(columnDef));
    this.rowDefs?.forEach((rowDef) => this.table?.addRowDef(rowDef));
    this.headerRowDefs?.forEach((headerRowDef) => this.table?.addHeaderRowDef(headerRowDef));

    // init grid state
    this.selection = new SelectionModel<T>(true, []);
    this.table?.setNoDataRow(this.noDataRow!);
  }

  ngOnInit(): void {
    if (!this.displayedColumns) {
      this.displayedColumns = this.displayColumnDefs.map((col) => col.key);
      // eslint-disable-next-line no-param-reassign,no-return-assign
      this.displayColumnDefs.forEach((col, index) => (col.index = index));
    }

    // Define a custom sorting function, because we want to sort case-insensitive!
    this.dataSource.sortingDataAccessor = (item: any, property) => {
      const value = item[property];
      return typeof value === 'string' ? value.toLowerCase() : value;
    };

    // Set default sorting if given via input property.
    if (this.sort && this.defaultSortingName) {
      this.sort.active = this.defaultSortingName;
      this.sort.direction = 'asc';
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['dataList']?.currentValue) {
      if (this.dataList) {
        this.dataSource.data = this.dataList;
      }
      if (this.sort) {
        this.dataSource.sort = this.sort;
      }
    }
    if (changes['displayColumnDefs']?.currentValue) {
      this.displayedColumns = this.displayColumnDefs.map((col) => col.key);
    }
  }

  selectRow(row: T): void {
    this.rowSelected.emit(row);
  }
}
