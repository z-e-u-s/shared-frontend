import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { LoadingService } from '../../services/loading-service/loading.service';

@Component({
  selector: 'lib-loading-progress',
  templateUrl: './loading-progress.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatProgressSpinner, AsyncPipe],
})
export class LoadingProgressComponent {
  private readonly loadingService = inject(LoadingService);

  isLoading = this.loadingService.isLoading;
}
