import { BreadcrumbItem } from '../public-api';

export const MOCK_BREADCRUMB_ITEMS: BreadcrumbItem[] = [
  { label: 'ADMINISTRATION', routerLink: '/administration', translate: true },
  { label: 'DETECTION_RULES.TITLE', routerLink: `/detection-rules`, translate: true },
  { label: 'DETECTION_RULE_TESTS.TITLE', translate: true },
];
