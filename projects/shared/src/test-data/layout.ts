import { BehaviorSubject } from 'rxjs';
import { Scheme } from '../modules/layout/models/scheme.types';
import { VersionInfo } from '../modules/layout/models/version-info.interface';

export const MOCK_VERSION_INFO: VersionInfo = {
  backendVersion: '1.0.0',
  frontendVersion: '1.0.0',
  sharedFrontendVersion: '1.0.0',
  logprepVersion: '1.0.0',
  frontendCommitHash: '34abcdef',
  backendCommitHash: 'abcdef12',
};

export const MOCK_SCHEME = new BehaviorSubject<Scheme>('light');
