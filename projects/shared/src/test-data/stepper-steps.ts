import { StepperStep } from '../lib/components/stepper/models/stepper-step.interface';

export const MOCK_STEPPER_STEPS: StepperStep[] = [
  {
    name: 'step1',
    title: 'STEPPER.TEST_DATA.STEP_1',
    isCompleted: false,
    isCurrent: false,
  },
  {
    name: 'step2',
    title: 'STEPPER.TEST_DATA.STEP_2',
    isCompleted: false,
    isCurrent: false,
  },
  {
    name: 'step3',
    title: 'STEPPER.TEST_DATA.STEP_3',
    isCompleted: false,
    isCurrent: false,
  },
];
