import { Component, Input } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'svg-icon',
  template: '<span></span>',
  standalone: true,
})
export class MockSvgIconComponent {
  @Input() src: any = null;
  @Input() applyClass: any = null;
}
