import { MenuItem } from '../lib/modules/layout/models/menu-item.model';

export const MOCK_MENU_ITEMS: MenuItem[] = [
  {
    group: 'SIDENAV.MAIN_MENU',
    separator: false,
    userRoles: [],
    children: [
      {
        icon: 'assets/icons/tablericons/home.svg',
        label: 'SIDENAV.DASHBOARD',
        route: 'dashboard',
        userRoles: [],
      },
      {
        icon: 'assets/icons/tablericons/folders.svg',
        label: 'SIDENAV.RELEASES',
        route: 'releases',
        userRoles: [],
        children: [
          {
            icon: 'assets/icons/tablericons/folder-plus.svg',
            label: 'SIDENAV.NEW_RELEASE',
            route: 'releases/new',
            userRoles: [],
          },
        ],
      },
      {
        icon: 'assets/icons/tablericons/tools.svg',
        label: 'SIDENAV.ADMINISTRATION',
        route: 'administration',
        userRoles: [],
      },
    ],
  },
];
