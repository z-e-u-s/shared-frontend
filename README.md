# SharedFrontend

This project holds and generates the Shared Frontend Library for the applications [FDA](https://gitlab.com/z-e-u-s/fda/fda-frontend) and [UCL](https://gitlab.com/z-e-u-s/ucl/ucl-frontend).

The library is located in `projects/shared`.

The application in `src` might be run for better testing of the shared objects and how they would work in an application.

## Development server

Run `npm install` to install all necessary dependencies.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

For a better development process, run `npm run build:library:watch` in another terminal to build the library.

## Build the Library

Run `npm run build:library` to build the library. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests from the library via Jest.

## husky & npm-check-updates

[`husky`](https://typicode.github.io/husky/) is used as a dev dependency package to run jobs as a pre-commit.

Make sure you have installed the `npm-check-updates` package on your machine in order to be able to use the `ncu` command.

If it's not the case, you can do it by running `npm install -g npm-check-updates`.

The `ncu` command is used by the task `npm-check-updates` (defined in `package.json`).

## Branching

The main development branch is the `main` branch.

If changes are to be made, either manually create a new feature branch or use the command `npm run new-feature $feature-name`. This command automatically increments the version both in the package.json and package-lock.json.
If the feature branch is created manually, the version may be incremented using `npm run increment-version`.

Use **Merge Requests** to merge the feature branch into `main`.

As soon as a merge request is created and the target branch is `main`, the pipeline automatically checks if the version has changed. If not, the pipeline will fail.

Once code is successfully merged to the `main` branch, it should also be pushed **manually** to a branch with the appropriate version number.

E.g. the version 1.0.0 on the main branch should be pushed to a new branch named `1.0.0` : `git checkout -b 1.0.0 && git push origin 1.0.0`.

## Usage and Versioning

Both applications [UCL](https://gitlab.com/z-e-u-s/ucl/ucl-frontend) and [FDA](https://gitlab.com/z-e-u-s/fda/fda-frontend) use the shared libabry as a git submodule.

To enable versioning with this submodule, these application should **never** use a feature branch or the `main` branch, but instead use a branch named with a specific version.
